package com.supertec.superofficeapp.ConstantClasses;

import android.app.PendingIntent;
import android.content.Intent;
import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.VolleyError;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.supertec.superofficeapp.SharedPreferences.NoInternetPref;
import com.supertec.superofficeapp.SharedPreferences.SettingsPref;
import com.supertec.superofficeapp.receivers.EmployeeLocationReceiver;
import com.supertec.superofficeapp.services.EmployeeLocationMonitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Programmer on 2/15/2018.
 */

public class EmployeeLocation {
    private static VolleyService mVolley;
    private static IResult mIResult;
    private FusedLocationProviderClient mFusedLocationClient;
    private static final String TAG = EmployeeLocation.class.getSimpleName();
    private GeofencingClient mGeofencingClient;
    List<Geofence> mGeo=new ArrayList<>();
    PendingIntent mGeofencePendingIntent=null;
    private static final EmployeeLocation ourInstance = new EmployeeLocation();

    public static EmployeeLocation getInstance() {
        return ourInstance;
    }

    private EmployeeLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(App.getContext());
        mGeofencingClient= LocationServices.getGeofencingClient(App.getContext());
        callBack();
        mVolley=new VolleyService(mIResult,App.getContext());
    }
    private GeofencingRequest getGeofencingRequest() {
        Log.d(TAG,App.getInstance().getPref().getHome_lat()+" "+App.getInstance().getPref().getHome_lng());
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(Geofence.GEOFENCE_TRANSITION_ENTER);
        Geofence office_geoffence=new Geofence.Builder().setRequestId("office")
                .setCircularRegion(
                        App.getInstance().getPref().getOffice_lat(), App.getInstance().getPref().getOffice_lng(),
                        50.0f
                )
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER|Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();
        Geofence home_geoffence=new Geofence.Builder().setRequestId("home")
                .setCircularRegion(
                        App.getInstance().getPref().getHome_lat(), App.getInstance().getPref().getHome_lng(),
                        50.0f
                )
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER|Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();
        mGeo.add(office_geoffence);
        mGeo.add(home_geoffence);
        builder.addGeofences(mGeo);
        return builder.build();
    }
    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.

        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(App.getContext(), EmployeeLocationReceiver.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getBroadcast(App.getContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }
    public void AddGeoFences(){
        mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                .addOnSuccessListener( new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG,"Success");
                        SettingsPref.getInstance().setEmployeeLocationisEnable(true);
                    }
                })
                .addOnFailureListener( new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG,"Fail "+e.getCause());
                    }
                });
    //checkFirstime();
    }
    private void checkFirstime(){
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        Log.d("Locationoutside","");
                        if(location!=null){
                            double office_lat=(double)App.getInstance()
                                    .getPref().getOffice_lat();
                            double office_lng=(double)App.getInstance()
                                    .getPref().getOffice_lng();
                            Location location1=new Location("");
                            location1.setLatitude(office_lat);
                            location1.setLongitude(office_lng);
                            float[] distance = new float[1];
                            Location.distanceBetween(office_lat,office_lng,location.getLatitude()
                                    ,location.getLongitude(),distance);
                            float distanceInMetersOne = location.distanceTo(location);
                            Log.d("Distance",distance[0]+"");
                            if(distance[0]<=25){
                                sendRequest("office");
                            }
                            else{
                                sendRequest("outside");
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("failure","");
            }
        });
    }
    public void removeGeoFences(){
        List<String> list=new ArrayList<>();
        list.add("office");
        list.add("home");
        mGeofencingClient.removeGeofences(list);
    }
    private void sendRequest(String status){
        if(ConnectivityCheck.getInstance().isConnected(App.getContext())){
            HashMap map=new HashMap();
            map.put("status",status);
            mVolley.postDataWithAuthorization("status",
                    "changestatus",map,App.getInstance().getPref().getToken());
        }
        else{
            NoInternetPref.getInstance().setRequest(status);
        }
    }
    private void callBack(){
        mIResult=new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                Log.d("mymy",response);
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {

            }

            @Override
            public void notifyNoInternet(String requestType, String url, HashMap map, String token) {

            }
        };
    }
}
