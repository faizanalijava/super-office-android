package com.supertec.superofficeapp.ConstantClasses;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;

/**
 * Created by Programmer on 3/8/2018.
 */

public class RunTimePermissions {
    private static final RunTimePermissions ourInstance = new RunTimePermissions();

    public static RunTimePermissions getInstance() {
        return ourInstance;
    }

    private RunTimePermissions() {
    }
    public static boolean runtimepermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(App.getContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
            {
                return false;
            }
        }
        return true;
    }
}
