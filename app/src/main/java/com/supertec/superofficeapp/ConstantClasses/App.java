package com.supertec.superofficeapp.ConstantClasses;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
//import android.support.multidex.MultiDex;
import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.GoogleApiAvailability;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.quickblox.auth.session.QBSettings;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.connections.tcp.QBTcpChatConnectionFabric;
import com.quickblox.chat.connections.tcp.QBTcpConfigurationBuilder;
import com.quickblox.chat.request.QBMessageGetBuilder;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.request.QBPagedRequestBuilder;
import com.quickblox.core.request.QBRequestBuilder;
import com.quickblox.core.request.QBRequestGetBuilder;
import com.quickblox.messages.QBPushNotifications;
import com.quickblox.messages.model.QBSubscription;
import com.quickblox.messages.services.QBPushManager;
import com.quickblox.messages.services.SubscribeService;
import com.quickblox.users.QBUsers;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import com.supertec.superofficeapp.services.EmployeeLocationMonitor;
import com.supertec.superofficeapp.services.PermanentService;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;

import io.socket.client.IO;
import io.socket.client.Socket;
import retrofit2.Retrofit;

/**
 * Created by Programmer on 1/14/2018.
 */

public class App extends Application {
    //    private RefWatcher refWatcher;
//    public static RefWatcher getRefwatcher(Context context){
//        AppController appController=(AppController) context.getApplicationContext();
//        return appController.refWatcher;
//    }
    public static final String TAG = App.class
            .getSimpleName();
    private QBMessageGetBuilder messageGetBuilder=null;
    private RequestQueue mRequestQueue;
    private QBRequestGetBuilder  mRequestBuilder=null;
    private QBPagedRequestBuilder pagedRequestBuilder=null;
    private ImageLoader mImageLoader;
    private Socket mSocket;
    private static App mInstance;
    private SharedPref pref=null;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        CreateDir();
        pref=new SharedPref(getApplicationContext());
        //LeakCanary.install(this);
//        Picasso.Builder builder = new Picasso.Builder(this);
//        builder.downloader(new OkHttp3Downloader(this,Integer.MAX_VALUE));
//        Picasso built = builder.build();
//        built.setLoggingEnabled(true);
//        Picasso.setSingletonInstance(built);
        try {
            mSocket = IO.socket(Constants.base_url);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        //initPushManager();
        //Toast.makeText(getApplicationContext(), ""+String.valueOf(QBSettings.getInstance().isEnablePushNotification()), Toast.LENGTH_SHORT).show();
    }

    public static synchronized App getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        Log.d("Tag",tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);

        }
    }
    public static Context getContext() {
        return mInstance.getApplicationContext();
    }
   // private Socket mSocket;
//    {
//        try {
//            mSocket = IO.socket(Constants.base_url);
//        } catch (URISyntaxException e) {
//            throw new RuntimeException(e);
//        }
//    }

    public Socket getSocket() {
        return mSocket;
    }
    public QBRequestGetBuilder  getBuilder(){
        if(mRequestBuilder==null){
             mRequestBuilder = new QBRequestGetBuilder();
        }
        return mRequestBuilder;
    }
    public QBMessageGetBuilder getMessageBuilder(){
        if(messageGetBuilder==null) {
            messageGetBuilder = new QBMessageGetBuilder();
            messageGetBuilder.setLimit(1000);
        }
        return messageGetBuilder;
    }
    public QBPagedRequestBuilder PageBuilder(){
        if(pagedRequestBuilder==null) {
            pagedRequestBuilder = new QBPagedRequestBuilder();
            pagedRequestBuilder.setPage(1);
            pagedRequestBuilder.setPerPage(50);
        }
        return pagedRequestBuilder;
    }
    private void CreateDir(){
        if(runtimepermission()) {
            File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), Constants.dir);
            //File mediaStorageDir = new File(getFilesDir(), Constants.dir);

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("App", "Exist"+""+mediaStorageDir);
                    mediaStorageDir.mkdir();
                }
            } else {
                Log.d("App", "not Exist");
            }
        }
    }
    private void initPushManager() {
        QBPushManager.getInstance().addListener(new QBPushManager.QBSubscribeListener() {
            @Override
            public void onSubscriptionCreated() {
                Log.d(TAG, "SubscriptionCreated");
                if(pref.getToken()==null){
                    SubscribeService.unSubscribeFromPushes(getApplicationContext());
                }
            }

            @Override
            public void onSubscriptionError(Exception e, int resultCode) {
                Log.d(TAG, "SubscriptionError" + e.getLocalizedMessage()+e.getStackTrace());
                if (resultCode >= 0) {
                    String error = GoogleApiAvailability.getInstance().getErrorString(resultCode);
                    Log.d(TAG, "SubscriptionError playServicesAbility: " + error);
                }

            }

            @Override
            public void onSubscriptionDeleted(boolean success) {
                Log.d("Deleted",String.valueOf(success));
                QBUsers.signOut().performAsync(new QBEntityCallback<Void>(){
                    @Override
                    public void onSuccess(Void result, Bundle args) {
                        pref.clearEverything();

                    }

                    @Override
                    public void onError(QBResponseException errors) {
                        Toast.makeText(getApplicationContext(), ""+errors.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });
    }
    public SharedPref getPref(){
        if(pref==null){
            pref=new SharedPref(getApplicationContext());
        }
        return pref;
    }
    private void chatSetup(){
        QBTcpConfigurationBuilder configurationBuilder = new QBTcpConfigurationBuilder().
        setKeepAlive(true) .
        setSocketTimeout(60).
        setUseTls(true).
        setPort(5222).
        setReconnectionAllowed(true).
        setAllowListenNetwork(true).
                setUseStreamManagement(false).
        setUseStreamManagementResumption(false).
        setAutojoinEnabled(false).
        setAutoMarkDelivered(true);
        QBChatService.setConnectionFabric(new QBTcpChatConnectionFabric(configurationBuilder));
        QBChatService.setDebugEnabled(true);
    }
    private boolean runtimepermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    ) {
                return false;
            }
        }
        return true;
    }
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }
}
