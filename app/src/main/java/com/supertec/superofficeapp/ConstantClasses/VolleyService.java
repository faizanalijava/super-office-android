package com.supertec.superofficeapp.ConstantClasses;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Programmer on 1/14/2018.
 */

public class VolleyService {
    IResult mResultCallback = null;
     Context mContext;
    DataSendingtoServer request;
    String request_type;
     MyResponseLister myResponseLister=new MyResponseLister();
    MyErrorlisterner myErrorlisterner=new MyErrorlisterner();

    public VolleyService(IResult resultCallback, Context context){

        mResultCallback = resultCallback;
        mContext = context;

    }

    public  void postDataWithAuthorization(final String requestType, String url, final HashMap map
            , final String token){
        if(ConnectivityCheck.getInstance().isConnected(mContext)) {
            request_type=requestType;
            try {

                 request = new DataSendingtoServer(Constants.getInstance().base_url + url, map
                         , myResponseLister,myErrorlisterner
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Authorization", "JWT " + token);
                        //Log.d("auth",headers.get("Authorization"));
                        return headers;
                    }
                };
                App.getInstance().addToRequestQueue(request, "request");


            } catch (Exception e) {

            }
        }
        else{
            mResultCallback.notifyNoInternet(requestType,url,map,token);
        }
    }
    public void postDataVolley(final String requestType, String url,HashMap map){
        request_type=requestType;
        try {

            request=new DataSendingtoServer(Constants.getInstance().base_url+url,map,
                    myResponseLister,myErrorlisterner
            );
            App.getInstance().addToRequestQueue(request,"request");


        }catch(Exception e){

        }
    }


    public void cancel(){
        Log.d("Destroy","cancel");
        request.cancel();
    }
    public  class MyResponseLister implements Response.Listener<String>{
        @Override
        public void onResponse(String response) {
            if(!request.isCanceled()) {
                if (response != null) {
                    Log.d("request",String.valueOf(request.isCanceled()));
                    mResultCallback.notifySuccess(request_type, response);
                }
            }
            else {
                Toast.makeText(App.getContext(), "Canceled", Toast.LENGTH_SHORT).show();
            }
        }
    }
    public class MyErrorlisterner implements Response.ErrorListener{
        @Override
        public void onErrorResponse(VolleyError error) {
            if(mResultCallback != null)
                mResultCallback.notifyError(request_type,error);
        }
    }


}