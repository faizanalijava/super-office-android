package com.supertec.superofficeapp.ConstantClasses;

import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Programmer on 1/15/2018.
 */

public interface UploadImage {
    @Multipart
    @POST("upload")
    Call<ResponseBody> uploadPhoto(
            @Part("authorization") RequestBody body,
            @Part MultipartBody.Part part
    );
}
