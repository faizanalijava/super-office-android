package com.supertec.superofficeapp.ConstantClasses;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Programmer on 1/15/2018.
 */

public class SharedPref {
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;


    public SharedPref(Context context) {
        preferences=context.getSharedPreferences("supertech",Context.MODE_PRIVATE);
        editor=preferences.edit();
    }
    public void setID(String id){
        editor.putString("user_id",id).commit();
    }
    public String getID(){
        return preferences.getString("user_id",null);
    }
    public void setToken(String token){
        editor.putString("token",token).commit();
    }
    public String getToken(){
        return preferences.getString("token",null);
    }
    public void setName(String name){
        editor.putString("name",name).commit();
    }
    public String getName(){
        return preferences.getString("name",null);
    }
    public void setEmail(String email){
        editor.putString("email",email).commit();
    }
    public String getEmail(){
        return preferences.getString("email",null);
    }
    public void setUser_Type(String user_type){
        editor.putString("user_type",user_type).commit();
    }
    public String getUser_Type(){
        return preferences.getString("user_type",null);
    }
    public void setImage_Url(String image_url){
        editor.putString("image_url",image_url).commit();
    }
    public String getImage_Url(){
        return preferences.getString("image_url",null);
    }

    public void setOffice_lat(Float lat){
        editor.putFloat("office_lat",lat).commit();
    }
    public Float getOffice_lat(){
        return preferences.getFloat("office_lat",0);
    }
    public void setOffice_lng(Float lng){
        editor.putFloat("office_lng",lng).commit();
    }
    public Float getOffice_lng(){
        return preferences.getFloat("office_lng",0);
    }
    public void setHome_lat(Float lat){
        editor.putFloat("home_lat",lat).commit();
    }
    public Float getHome_lat(){
        return preferences.getFloat("home_lat",0);
    }
    public void setHome_lng(Float lng){
        editor.putFloat("home_lng",lng).commit();
    }
    public Float getHome_lng(){
        return preferences.getFloat("home_lng",0);
    }
    public void setQbid(String id){
        editor.putString("qb_id",id).commit();
    }
    public String getQbid(){
        return preferences.getString("qb_id",null);
    }
    public void setPass(String pass){
        editor.putString("pass",pass).commit();
    }
    public String getPass(){
        return preferences.getString("pass",null);
    }
    public void setCurrentDialogid(String id){
        editor.putString("dialog_id",id).commit();
    }
    public String getDialogId(){
        return preferences.getString("dialog_id",null);
    }
    public void setLastStatus(String status){
        editor.putString("last_status",status).commit();
    }
    public String getLastStatus(){
        return preferences.getString("last_status","null");
    }
    public void clearEverything(){
        editor.clear().commit();
    }

}
