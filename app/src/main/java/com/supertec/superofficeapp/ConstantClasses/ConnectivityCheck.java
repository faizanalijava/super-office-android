package com.supertec.superofficeapp.ConstantClasses;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Programmer on 1/19/2018.
 */

public class ConnectivityCheck {
    private static final ConnectivityCheck ourInstance = new ConnectivityCheck();
    public  boolean isConnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
    public static ConnectivityCheck getInstance() {
        return ourInstance;
    }
    private ConnectivityCheck(){}

}
