package com.supertec.superofficeapp.ConstantClasses;

import android.os.Environment;

import java.io.File;

/**
 * Created by Programmer on 1/14/2018.
 */

public class Constants {
    private static final Constants ourInstance = new Constants();
    //public static String base_url="http://10.0.3.2:4000/";
    //public static String base_url="http://192.168.1.14:4000/";
    public static String dir="Superoffice";
    public static boolean isInitialize=false;
    public static boolean isInBackground=true;
    public static String currentDialog="";
    public static String fullpath= Environment.getExternalStorageDirectory()
            + File.separator+Constants.dir+File.separator;
    //public static String fullpath= App.getInstance().getApplicationContext().getFilesDir().toString();
    public static String base_url="https://supertech-jxnenterprises.c9users.io/";

    public static Constants getInstance() {
        return ourInstance;
    }

    private Constants() {
    }
}
