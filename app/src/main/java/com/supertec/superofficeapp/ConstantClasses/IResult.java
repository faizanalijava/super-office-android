package com.supertec.superofficeapp.ConstantClasses;

import com.android.volley.VolleyError;

import java.util.HashMap;

/**
 * Created by Programmer on 1/14/2018.
 */

public interface IResult {
    public void notifySuccess(String requestType,String response);
    public void notifyError(String requestType,VolleyError error);
    public void notifyNoInternet(String requestType, String url,  HashMap map
            , String token);
}