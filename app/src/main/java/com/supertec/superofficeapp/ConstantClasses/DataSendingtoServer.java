package com.supertec.superofficeapp.ConstantClasses;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Programmer on 1/14/2018.
 */

public class DataSendingtoServer extends StringRequest {

    private Map params;



    public DataSendingtoServer(String url, HashMap map, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, url, listener, errorListener);
        params = new HashMap();
        params.putAll(map);


    }




    @Override
    public Map<String, String> getParams() {
        return params;
    }

    private void getValuesFromHashMap(Map<String, String> hashMap) {

        List<String> values = new ArrayList<String>();

        for (String item : hashMap.values()) {
            values.add(item);
        }

        for (int i = 0; i < values.size(); i++) {
            Log.d("values", "" + values.get(i));
        }
    }
}