package com.supertec.superofficeapp.ConstantClasses;

import android.os.Bundle;
import android.util.Log;

import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogCustomData;
import com.quickblox.chat.request.QBDialogRequestBuilder;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Programmer on 4/4/2018.
 */

public class UpdateConversation {
    private static final UpdateConversation ourInstance = new UpdateConversation();

    public static UpdateConversation getInstance() {
        return ourInstance;
    }

    private UpdateConversation() {
    }
    public void update(){
        QBRestChatService.getChatDialogs(null, App.getInstance().getBuilder())
                .performAsync(new QBEntityCallback<ArrayList<QBChatDialog>>() {
                    @Override
                    public void onSuccess(ArrayList<QBChatDialog> qbChatDialogs, Bundle bundle) {
                        List<QBChatDialog> owner_list=new ArrayList<>();
                        List<QBChatDialog> not_owner_list=new ArrayList<>();
                        for(int i=0;i<qbChatDialogs.size();i++){
                            if(qbChatDialogs.get(i).getUserId().toString().equals(App.getInstance().getPref().getQbid())){
                                QBDialogCustomData data=new QBDialogCustomData("custom");
                                data.putString("owner_image",App.getInstance().getPref().getImage_Url());
                                qbChatDialogs.get(i).setCustomData(data);
                                owner_list.add(qbChatDialogs.get(i));
                            }
                            else{
                                qbChatDialogs.get(i).setPhoto(App.getInstance().getPref().getImage_Url());
                                not_owner_list.add(qbChatDialogs.get(i));
                            }
                        }
                        if(owner_list.size()>0){
                            QBDialogRequestBuilder requestBuilder = new QBDialogRequestBuilder();
                            for(int i=0;i<owner_list.size();i++){
                                QBRestChatService.updateGroupChatDialog(owner_list.get(i),requestBuilder)
                                        .performAsync(new QBEntityCallback<QBChatDialog>() {
                                            @Override
                                            public void onSuccess(QBChatDialog dialog, Bundle bundle) {
                                            }

                                            @Override
                                            public void onError(QBResponseException e) {

                                            }
                                        });
                            }
                        }
                         if(not_owner_list.size()>0){

                            QBDialogRequestBuilder requestBuilder = new QBDialogRequestBuilder();
                            for(int i=0;i<not_owner_list.size();i++){
                                QBRestChatService.updateGroupChatDialog(not_owner_list.get(i),requestBuilder)
                                        .performAsync(new QBEntityCallback<QBChatDialog>() {
                                            @Override
                                            public void onSuccess(QBChatDialog dialog, Bundle bundle) {

                                            }

                                            @Override
                                            public void onError(QBResponseException e) {
                                            }
                                        });
                            }
                        }
                    }

                    @Override
                    public void onError(QBResponseException e) {
                    }
                });
    }
}
