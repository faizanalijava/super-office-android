package com.supertec.superofficeapp.ConstantClasses;

import android.os.Bundle;
import android.util.Log;

import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;

/**
 * Created by Programmer on 2/15/2018.
 */

public class ChatLogin {
    private static final ChatLogin ourInstance = new ChatLogin();

    public static ChatLogin getInstance() {
        return ourInstance;
    }

    private ChatLogin() {
    }
    public static void Login() {
        if (App.getInstance().getPref().getToken() != null) {
            QBUser user = new QBUser();
            user.setLogin(App.getInstance().getPref().getEmail());
            user.setPassword(App.getInstance().getPref().getPass());
            user.setId(Integer.parseInt(App.getInstance().getPref().getQbid()));
            QBChatService.getInstance().login(user, new QBEntityCallback() {
                @Override
                public void onSuccess(Object o, Bundle bundle) {
                    Log.d("Major", "Success");
                }

                @Override
                public void onError(QBResponseException errors) {
                    Log.d("Major", errors.getMessage());
                }
            });
        }
    }
}
