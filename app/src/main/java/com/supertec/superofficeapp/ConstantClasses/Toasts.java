package com.supertec.superofficeapp.ConstantClasses;

import android.widget.Toast;

/**
 * Created by Programmer on 2/12/2018.
 */

public class Toasts {
    private static final Toasts ourInstance = new Toasts();

    public static Toasts getInstance() {
        return ourInstance;
    }

    private Toasts() {
    }
    public void ShowNoConnectivityToast(){
        Toast.makeText(App.getContext(), "No Internet Found", Toast.LENGTH_LONG).show();
    }
}
