package com.supertec.superofficeapp.ConstantClasses;

import android.util.Log;

import com.quickblox.auth.session.QBSettings;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.connections.tcp.QBTcpChatConnectionFabric;
import com.quickblox.chat.connections.tcp.QBTcpConfigurationBuilder;

/**
 * Created by Programmer on 2/15/2018.
 */

public class ChatSetup {
    private static final ChatSetup ourInstance = new ChatSetup();

    public static ChatSetup getInstance() {
        return ourInstance;
    }

    private ChatSetup() {
    }
    public void InitializeChat(){
        QBSettings.getInstance().init(App.getContext(), "67172", "GjEmrff-2XJDFO2", "q9kvsKOsUhtUq8j");
        QBSettings.getInstance().setAccountKey("1AKQRu-EPTtPxzxxDrBo");
        QBSettings.getInstance().setAutoCreateSession(true);
            QBChatService.setDefaultPacketReplyTimeout(15000);
        QBChatService.setDefaultAutoSendPresenceInterval(60); //Sets interval to send presence to server to keep connection active.
        QBChatService.setDefaultConnectionTimeout(30000); //Sets how long the socket will wait until a TCP connection is established (in milliseconds).
        chatSetup();
        Constants.isInitialize=true;
    }
    private void chatSetup(){
        QBTcpConfigurationBuilder configurationBuilder = new QBTcpConfigurationBuilder().
                setKeepAlive(true).
                setSocketTimeout(60).
                setUseTls(true).
                setPort(5222).
                setReconnectionAllowed(true).
                setAllowListenNetwork(true).
                setUseStreamManagement(false).
                setUseStreamManagementResumption(false).
                setAutojoinEnabled(false).
                setAutoMarkDelivered(true);
        QBChatService.setConnectionFabric(new QBTcpChatConnectionFabric(configurationBuilder));
        QBChatService.setDebugEnabled(true);
    }
}
