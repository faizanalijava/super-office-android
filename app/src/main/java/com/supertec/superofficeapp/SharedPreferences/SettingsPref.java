package com.supertec.superofficeapp.SharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;

/**
 * Created by Programmer on 1/25/2018.
 */

public class SettingsPref {
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;
    private static final SettingsPref ourInstance = new SettingsPref();

    public static SettingsPref getInstance() {
        return ourInstance;
    }

    private SettingsPref() {
        preferences= App.getContext().getSharedPreferences("settings", Context.MODE_PRIVATE);
        editor=preferences.edit();
    }
    public void setLocationSharing(boolean sharing){
        editor.putBoolean("location_sharing",sharing).commit();
    }
    public boolean getLocationSharing(){
        return preferences.getBoolean("location_sharing",true);
    }
    public void setEmployeeLocationisEnable(boolean sharing){
        editor.putBoolean("employee_geofence",sharing).commit();
    }
    public boolean getEmployeeLocationisEnable(){
        return preferences.getBoolean("employee_geofence",false);
    }
}
