package com.supertec.superofficeapp.SharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.supertec.superofficeapp.ConstantClasses.App;

/**
 * Created by Programmer on 1/21/2018.
 */

public class NoInternetPref {
    private static SharedPreferences NoInternet;
    private static SharedPreferences.Editor editor;
    private static final NoInternetPref ourInstance = new NoInternetPref();

    public static NoInternetPref getInstance() {
        return ourInstance;
    }

    private NoInternetPref() {
        NoInternet= App.getContext().getSharedPreferences("nointernet", Context.MODE_PRIVATE);
        editor=NoInternet.edit();
    }
    public void setRequest(String request){
        editor.putString("request",request).commit();
    }
    public String getRequest(){
        return NoInternet.getString("request",null);
    }
    public void clearEverything(){
        editor.clear().commit();
    }
}
