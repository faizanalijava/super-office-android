package com.supertec.superofficeapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Programmer on 1/16/2018.
 */

public class EmployeeModel implements Serializable {
    @SerializedName("_id")
    String id;
    @SerializedName("name")
     String mEmployeeName;
    @SerializedName("image_url")
     String mEmployeeImageUrl;
    @SerializedName("email")
     String mEmployeeemail;
    @SerializedName("current_status")
    String mEmployeeCurrentState;
    @SerializedName("qb_id")
    String qb_id;

    public String getQb_id() {
        return qb_id;
    }

    public void setQb_id(String qb_id) {
        this.qb_id = qb_id;
    }
    //private String mEmployeeCurrentState;


    public EmployeeModel(String id, String mEmployeeName, String mEmployeeImageUrl, String mEmployeeemail) {
        this.id = id;
        this.mEmployeeName = mEmployeeName;
        this.mEmployeeImageUrl = mEmployeeImageUrl;
        this.mEmployeeemail = mEmployeeemail;
    }

    public String getmEmployeeCurrentState() {
        return mEmployeeCurrentState;
    }

    public void setmEmployeeCurrentState(String mEmployeeCurrentState) {
        this.mEmployeeCurrentState = mEmployeeCurrentState;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getmEmployeeName() {
        return mEmployeeName;
    }

    public String getmEmployeeemail() {
        return mEmployeeemail;
    }

    public void setmEmployeeemail(String mEmployeeemail) {
        this.mEmployeeemail = mEmployeeemail;
    }

    public void setmEmployeeName(String mEmployeeName) {
        this.mEmployeeName = mEmployeeName;
    }

    public String getmEmployeeImageUrl() {
        return mEmployeeImageUrl;
    }

    public void setmEmployeeImageUrl(String mEmployeeImageUrl) {
        this.mEmployeeImageUrl = mEmployeeImageUrl;
    }

//    public String getmEmployeeCurrentState() {
//        return mEmployeeCurrentState;
//    }
//
//    public void setmEmployeeCurrentState(String mEmployeeCurrentState) {
//        this.mEmployeeCurrentState = mEmployeeCurrentState;
//    }
}
