package com.supertec.superofficeapp.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Programmer on 1/17/2018.
 */

public class TaskModel implements Serializable {
    @SerializedName("_id")
    String task_id;
    String task_name;
    @SerializedName("task_description")
    String task_Description;
    String task_date;
    @SerializedName("user")
    String user_id;
    @SerializedName("task_status")
    String task_status;
    @SerializedName("updates")
    List<TaskUpdates> list;

    public String getTask_status() {
        return task_status;
    }

    public void setTask_status(String task_status) {
        this.task_status = task_status;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public String getTask_Description() {
        return task_Description;
    }

    public void setTask_Description(String task_Description) {
        this.task_Description = task_Description;
    }

    public String getTask_date() {
        return task_date;
    }

    public void setTask_date(String task_date) {
        this.task_date = task_date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public List<TaskUpdates> getList() {
        return list;
    }

    public void setList(List<TaskUpdates> list) {
        this.list = list;
    }

    public static class TaskUpdates implements Serializable{
        @SerializedName("update_date")
        String task_update_date;
        @SerializedName("update_description")
        String task_update_Description;

        public String getTask_update_date() {
            return task_update_date;
        }

        public void setTask_update_date(String task_update_date) {
            this.task_update_date = task_update_date;
        }

        public String getTask_update_Description() {
            return task_update_Description;
        }

        public void setTask_update_Description(String task_update_Description) {
            this.task_update_Description = task_update_Description;
        }
    }
}
