package com.supertec.superofficeapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.models.TaskModel;

import java.util.List;

/**
 * Created by Programmer on 1/18/2018.
 */

public class Task_update_Adapter extends RecyclerView.Adapter<
        Task_update_Adapter.MyViewHolder> {
    List<TaskModel.TaskUpdates> updates;
    int size=0;
    public void setUpdates(List<TaskModel.TaskUpdates> updates) {
        this.updates = updates;
        size=updates.size();
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView= LayoutInflater.from(parent.getContext()).inflate(R.layout.task_update_item,
                parent,false);
        return new MyViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String[] split=updates.get(position).getTask_update_date().split("T");
        //int n=position+1;
        //holder.txt_update_no.setText("Update "+n);
        holder.txt_update_date.setText(split[0]);
        holder.txt_update_description.setText(updates.get(position).getTask_update_Description());
    }

    @Override
    public int getItemCount() {
        return size;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txt_update_no,txt_update_date,txt_update_description;
        public MyViewHolder(View itemView) {
            super(itemView);
//            txt_update_no=(TextView) itemView.findViewById(R.id.txt_update_no);
            txt_update_date=(TextView) itemView.findViewById(R.id.txt_update_date);
            txt_update_description=(TextView) itemView.findViewById(R.id.txt_update_description);
        }
    }
}
