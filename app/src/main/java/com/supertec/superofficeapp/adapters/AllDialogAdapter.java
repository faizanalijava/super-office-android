package com.supertec.superofficeapp.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogCustomData;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.ChatLogin;
import com.supertec.superofficeapp.ConstantClasses.ChatSetup;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.activities.ChatActivity;
import com.supertec.superofficeapp.activities.EmployeeActivities.EmployeeDashboard;
import com.supertec.superofficeapp.services.AllMessagesListener;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Programmer on 2/6/2018.
 */

public class AllDialogAdapter extends RecyclerView.Adapter<AllDialogAdapter.AllDiaglogHolder> {
    List<QBChatDialog> list;
    Context mContext;

    public AllDialogAdapter(Context mContext) {
        this.mContext = mContext;
        list=new ArrayList<>();
    }

    public void setList(List<QBChatDialog> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public AllDiaglogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(
                R.layout.dialog_item,parent,false
        );
        return new AllDiaglogHolder(view);
    }

    @Override
    public void onBindViewHolder(AllDiaglogHolder holder, final int position) {
       // Toast.makeText(mContext, "here1", Toast.LENGTH_SHORT).show();
        holder.txt_unread.setVisibility(View.GONE);
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String today = df.format(c.getTime());
        c.add(Calendar.DATE,-1);
        Log.d("PlayingWithDates",String.valueOf(list.get(position).getLastMessageDateSent()));
        String yesterday = df.format(c.getTime());
        if(!(String.valueOf(list.get(position).getLastMessageDateSent()).equals("0"))) {
            if (new SimpleDateFormat("MM/dd/yyyy").format(new Date(list.get(position
            ).getLastMessageDateSent() * 1000L)).toString()
                    .equals(today)) {
                holder.txt_last_date.setText("Today");
            } else if (new SimpleDateFormat("MM/dd/yyyy").format(new Date(list.get(position
            ).getLastMessageDateSent() * 1000L)).toString()
                    .equals(yesterday)) {
                holder.txt_last_date.setText("Yesterday");
            } else {
                holder.txt_last_date.setText(
                        new SimpleDateFormat("MMM d h:mm a").format(new Date(list.get(position
                        ).getLastMessageDateSent() * 1000L)));
            }
        }
        holder.txt_name.setText(list.get(position).getName());
        if(list.get(position).getUnreadMessageCount()>0){
            holder.txt_unread.setVisibility(View.VISIBLE);
            holder.txt_unread.setText(list.get(position).getUnreadMessageCount().toString());
        }
        if(list.get(position).getLastMessage()!=null&&list.get(position).getLastMessage().length()>30) {
            holder.txt_last_message.setText(list.get(position).getLastMessage().substring(0,30)+" ...");
        }
        else{
            holder.txt_last_message.setText(list.get(position).getLastMessage());
        }
        if(App.getInstance().getPref().getQbid().equals(list.get(position).getUserId().toString())){
            Glide.with(App.getContext()).load(Constants.base_url+list.get(position).getPhoto())
                    .into(holder.img);
        }
        else{
            QBDialogCustomData customData=list.get(position).getCustomData();
            Log.d("Data",Constants.base_url+customData.getFields().get("owner_image").toString());
            Glide.with(App.getContext()).load(Constants.base_url+customData.getFields().get("owner_image").toString())
                    .into(holder.img);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //
                if(QBChatService.getInstance().isLoggedIn()) {
                    Intent intent = new Intent(App.getContext(), ChatActivity.class);
                    intent.putExtra("EXTRA_DIALOG", list.get(position));
                    intent.putExtra("check", "fromdialog");
                    if(Build.VERSION.SDK_INT<=23){
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    //Toast.makeText(mContext, "here"+QBChatService.getInstance().isLoggedIn(), Toast.LENGTH_SHORT).show();
                    mContext.startActivity(intent);
                }
                else{
                    ChatSetup.getInstance().InitializeChat();
                    loginToChat(list.get(position));
                }

            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        QBRestChatService.deleteDialog(list
                                .get(position).getDialogId(), false).performAsync(new QBEntityCallback<Void>() {
                            @Override
                            public void onSuccess(Void aVoid, Bundle bundle) {
                                list.remove(position);
                                notifyDataSetChanged();

                            }

                            @Override
                            public void onError(QBResponseException e) {

                            }
                        });
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                    }
                });

                builder.setMessage("Are you sure you want to delete this conversation");
                builder.setTitle("Delete Converstation");

                AlertDialog d = builder.create();
                d.show();
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class AllDiaglogHolder extends RecyclerView.ViewHolder{
        TextView txt_name,txt_last_date,txt_last_message,txt_unread;
        CircleImageView img;
        public AllDiaglogHolder(View itemView) {
            super(itemView);
            txt_name=(TextView) itemView.findViewById(R.id.txt_name);
            txt_last_date=(TextView) itemView.findViewById(R.id.txt_date);
            txt_last_message=(TextView) itemView.findViewById(R.id.txt_lastmessage);
            txt_unread=(TextView) itemView.findViewById(R.id.unread_messages);
            img=(CircleImageView) itemView.findViewById(R.id.user_img);
        }
    }
    private void loginToChat(final QBChatDialog dialog){
        QBUser user = new QBUser();
        user.setLogin(App.getInstance().getPref().getEmail());
        user.setPassword(App.getInstance().getPref().getPass());
        user.setId(Integer.parseInt(App.getInstance().getPref().getQbid()));
        QBChatService.getInstance().login(user, new QBEntityCallback() {
            @Override
            public void onSuccess(Object o, Bundle bundle) {
                Intent intent = new Intent(mContext, ChatActivity.class);
                intent.putExtra("EXTRA_DIALOG", dialog);
                intent.putExtra("check", "fromdialog");
                if(Build.VERSION.SDK_INT<=23){
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                mContext.startActivity(intent);
            }

            @Override
            public void onError(QBResponseException errors) {
                Log.w("Major1",errors.getMessage()+errors.getStackTrace()+errors.getCause());
            }
        });
    }
}
