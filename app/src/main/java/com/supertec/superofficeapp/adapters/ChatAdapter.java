package com.supertec.superofficeapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.content.QBContent;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBProgressCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.io.ByteStreams;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.ChatMessage;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.R;
import org.jivesoftware.smack.util.Async;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Programmer on 2/6/2018.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int VIEW_TYPE_MESSAGE_SENT = 0;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 1;
    private static int last=0;
    List<QBChatMessage> list;
    String id;

    public ChatAdapter(String id) {
        this.id = id;
        list=new ArrayList<>();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void setList(List<QBChatMessage> list) {
        this.list = list;
        notifyDataSetChanged();
        Log.d("hyy","hyy");
    }
    public void changeMessageStatus(String message_id,int user_id,int status){
        if(status==1) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getId().equals(message_id)) {
                    List<Integer> deliverids=new ArrayList<>();
                    deliverids.add(user_id);
                    list.get(i).setDeliveredIds(deliverids);
                }
            }
        }
        else{
            for(int i=0;i<list.size();i++){
                if(list.get(i).getId().equals(message_id)){
                    List<Integer> readids=new ArrayList<>();
                    readids.add(user_id);
                    list.get(i).setReadIds(readids);
                    notifyDataSetChanged();
                    break;
                }
            }
        }

    }
    public void addNewMessage(QBChatMessage message){
        list.add(message);
        notifyItemInserted(list.size()-1);

    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        //holder.setIsRecyclable(false);
        QBChatMessage message=list.get(position);

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        String today = df.format(c.getTime());
//        Log.d("Dates",today+"  "+
//                new SimpleDateFormat("MM/dd/yyyy").format(new Date(list.get(position
//                ).getDateSent() * 1000L)).toString());
        switch (holder.getItemViewType()){
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);

                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public int getItemViewType(int position) {
        QBChatMessage message = (QBChatMessage) list.get(position);
        if (message.getSenderId().toString().equals(id)) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;

        } else {
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }

    }



    public  class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, nameText,statusText;
        CircleImageView profileImage;
        ImageView img;

        ReceivedMessageHolder(View itemView) {
            super(itemView);
            messageText = (TextView) itemView.findViewById(R.id.text_message_body1);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
            nameText = (TextView) itemView.findViewById(R.id.text_message_name);
            profileImage = (CircleImageView) itemView.findViewById(R.id.image_message_profile);
            img = (ImageView) itemView.findViewById(R.id.img);
            statusText = (TextView) itemView.findViewById(R.id.txt_status);
        }

        void bind(QBChatMessage message) {
            postProcessingReceived(message);
        }
private void fileProcessingReceived( final File file, String type, final String name){
    messageText.setText("");
    if(type.equals("photo")) {
        messageText.setText("");
        messageText.setVisibility(View.GONE);
        messageText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        img.layout(0,0,0,0);
        Glide.with(App.getContext()).load(Uri.fromFile(file)).into(img);
        //img.setImageURI(Uri.fromFile(file));
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openFile(attachment.getName());
                openDocument(name,file);
            }
        });
    }
    else if(type.equals("document")||type.equals("audio")){
        messageText.setVisibility(View.VISIBLE);
        img.setImageDrawable(null);
        if(type.equals("audio")){
            messageText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_play,
                    0,0,0);
        }
        else if(type.equals("document")){
            messageText
                    .setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_document,
                            0,0,0);
        }
        messageText.setText(name);
        messageText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDocument(name,file);
            }
        });

    }
}
        private void postProcessingReceived( final QBChatMessage message){
            Calendar c = Calendar.getInstance();

            SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            String today = df.format(c.getTime());
            if(message.getAttachments()!=null&&message.getAttachments().size()>0){
                for(final QBAttachment attachment:message.getAttachments()){
//                        Log.d("Attchment",attachment.getName()+" "+attachment.getContentType()+" "+
//                                attachment.getType()+attachment.getUrl()+"  "+attachment.getId());
                    final File file=new File(Constants.fullpath+attachment.getName());
                    if(file.exists()) {
//                        Log.d("File",file.getAbsolutePath());
                        fileProcessingReceived(file,attachment.getType(),attachment.getName());
                    }
                    else {
                        QBContent.downloadFileById(Integer.parseInt(attachment.getId()), new QBProgressCallback() {
                            @Override
                            public void onProgressUpdate(int i) {

                            }
                        }).performAsync(new QBEntityCallback<InputStream>() {
                            @Override
                            public void onSuccess(InputStream inputStream, Bundle bundle) {
                                FileDownloader downloader=new FileDownloader(file,attachment,
                                        inputStream,ReceivedMessageHolder.this,"receive");
                                downloader.execute();
//                                if(saveFile(inputStream, attachment.getName())){
//                                    fileProcessingReceived(file,attachment.getType(),attachment.getName());
//                                }

                            }

                            @Override
                            public void onError(QBResponseException e) {

                            }
                        });
                    }
                }
            }
            else{
                img.setImageDrawable(null);
                messageText.setVisibility(View.VISIBLE);
                messageText.setText(message.getBody().toString());
                messageText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                messageText.setOnClickListener(null);
            }
//                if(message.getBody()!=null) {
//
//                }
            nameText.setText(message.getProperty("name").toString());
            //holder.statusText.setText(message.getProperty("status").toString());
            timeText.setText(String.valueOf(message.getDateSent()));
            Glide.with(App.getContext()).load(Constants.base_url+message.getProperty("image")).into(profileImage);
            if(new SimpleDateFormat("MM/dd/yyyy").format(new Date(message
                    .getDateSent() * 1000L)).toString()
                    .equals(today)){
                timeText.setText(
                        new SimpleDateFormat("h:mm a").format(new Date(message
                                .getDateSent() * 1000L))
                );
            }
            else{
                timeText.setText(
                        new SimpleDateFormat("MMM d h:mm a").format(new Date(message
                                .getDateSent() * 1000L))
                );

            }
        }
    }
    public  class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText,statusText;
        ImageView img;

        SentMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
            statusText = (TextView) itemView.findViewById(R.id.txt_status);
            img = (ImageView) itemView.findViewById(R.id.img);
        }

        void bind(QBChatMessage message) {
            timeText.setText("");
            statusText.setText("");
            postProcessingSent(message);
        }
        private void postProcessingSent(final QBChatMessage message){
    Calendar c = Calendar.getInstance();

    SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
    String today = df.format(c.getTime());
    if(message.getAttachments()!=null&&message.getAttachments().size()>0) {

        for (final QBAttachment attachment : message.getAttachments()) {
//            Log.d("Attchment", attachment.getName() + " " + attachment.getContentType() + " " +
//                    attachment.getType() + attachment.getUrl() + "  " + attachment.getId());
            final File file = new File(message.getProperty("file_path").toString());
            final File file1=new File(Constants.fullpath+attachment.getName());
            if (file.exists()) {
                fileProcessingSent(file,attachment.getType(),attachment.getName());

            }
            else if (file1.exists()) {

                fileProcessingSent(file1,attachment.getType(),attachment.getName());
            }
            else {
                QBContent.downloadFileById(Integer.parseInt(attachment.getId()), new QBProgressCallback() {
                    @Override
                    public void onProgressUpdate(int i) {

                    }
                }).performAsync(new QBEntityCallback<InputStream>() {
                    @Override
                    public void onSuccess(InputStream inputStream, Bundle bundle) {
                        final File downloaded_file=new File(Constants.fullpath+
                                attachment.getName());
                        FileDownloader downloader=new FileDownloader(downloaded_file,attachment,
                                inputStream,"sent",SentMessageHolder.this);
                        downloader.execute();
//                        if(saveFile(inputStream, attachment.getName())){
//
//                            fileProcessingSent(downloaded_file,attachment.getType(),attachment.getName());
//                        }
                    }

                    @Override
                    public void onError(QBResponseException e) {

                    }
                });
            }
        }
    }
    else{
        img.setImageDrawable(null);
        messageText.setText(message.getBody().toString());
        messageText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
        messageText.setOnClickListener(null);
    }

    if(new SimpleDateFormat("MM/dd/yyyy").format(new Date(message.getDateSent() * 1000L)).toString()
            .equals(today)){
        timeText.setText(
                new SimpleDateFormat("h:mm a").format(new Date(message.getDateSent() * 1000L))
        );
    }
    else{
        timeText.setText(
                new SimpleDateFormat("MMM d h:mm a").format(new Date(message.getDateSent() * 1000L))
        );
    }
    if(message.getReadIds()!=null&&message.getReadIds().contains(message.getRecipientId())){

        statusText.setText("Seen");
    }
    else{
        statusText.setText("Sent");
    }

}
        private void fileProcessingSent( final File file, String type, final String name){
            if(type.equals("photo")) {
                messageText.setText("");
                messageText.setCompoundDrawablesWithIntrinsicBounds(0,0,0,0);
                img.layout(0,0,0,0);
                Glide.with(App.getContext()).load(Uri.fromFile(file)).into(img);

                img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //openFile(attachment.getName());
                        openDocument(name,file);
                    }
                });
            }
            else if(type.equals("document")||type.equals("audio")){
                img.setImageDrawable(null);
                if(type.equals("audio")){
                    messageText.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_play,
                            0,0,0);
                }
                else if(type.equals("document")){
                    messageText
                            .setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_action_document,
                                    0,0,0);
                }
                messageText.setText(name);
                messageText.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openDocument(name,file);
                    }
                });

            }
        }
    }
    private boolean saveFile(InputStream inputStream, final String filename){
//        Log.d("Stream",inputStream.toString());
        File file=new File(Constants.fullpath+
        filename);
//        try {
//           // Log.d("Path",file.getAbsolutePath().toString()+ String.valueOf(inputStream.available()));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            OutputStream fos = new FileOutputStream(file);
            //com.quickblox.core.io.IOUtils.copy(inputStream,fos);
            byte[] buffer = new byte[4096*2];//Set buffer type
//            int len1 = 0;//init length
//            while ((len1 = inputStream.read(buffer)) != -1) {
//                fos.write(buffer, 0, len1);//Write new file
//            }
//
//            //Close all connection after doing task
//            fos.close();
            //inputStream.close();
            long fileSizeDownloaded=0;
            while (true) {
                int read = inputStream.read(buffer);

                if (read == -1) {
                    break;
                }

                fos.write(buffer, 0, read);

                fileSizeDownloaded += read;

                Log.d("File", "file download: " + fileSizeDownloaded + " of " + "");
            }
            fos.flush();
            fos.close();
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
//        BufferedInputStream bis = new BufferedInputStream(inputStream);
//        FileOutputStream fos = null;
//        try {
//            fos = new FileOutputStream(file);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        byte buffer[] = new byte[1024];
//        int read;
//        try {
//            while ((read = bis.read(buffer)) > 0) {
//                try {
//                    fos.write(buffer, 0, read);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            fos.flush();
//            fos.close();
//            bis.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }



    public void openDocument(String filename,File url) {

        Uri uri = Uri.fromFile(url);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        if (url.toString().contains(".doc") || url.toString().contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword");
        } else if (url.toString().contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf");
        } else if (url.toString().contains(".ppt") || url.toString().contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint");
        } else if (url.toString().contains(".xls") || url.toString().contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel");
        } else if (url.toString().contains(".zip") || url.toString().contains(".rar")) {
            // WAV audio file
            intent.setDataAndType(uri, "application/x-wav");
        } else if (url.toString().contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf");
        } else if (url.toString().contains(".wav") || url.toString().contains(".mp3") || url.toString().contains(".aac")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav");
        } else if (url.toString().contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif");
        } else if (url.toString().contains(".jpg") || url.toString().contains(".jpeg") || url.toString().contains(".png")) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg");
        } else if (url.toString().contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain");
        } else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") || url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
            // Video files
            intent.setDataAndType(uri, "video/*");
        } else {
            //if you want you can also define the intent type for any other file
            //additionally use else clause below, to manage other unknown extensions
            //in this case, Android will show all applications installed on the device
            //so you can choose which application to use
            intent.setDataAndType(uri, "*/*");
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (intent.resolveActivity(App.getContext().getPackageManager()) != null) {
            App.getContext().startActivity(intent);
        }
        else{
            Toast.makeText(App.getContext(), "No App Found for this task", Toast.LENGTH_SHORT).show();
        }


    }



    @Override
    public long getItemId(int position) {
//        QBChatMessage message = (QBChatMessage) list.get(position);
//        if (message.getSenderId().toString().equals(id)) {
//            // If the current user is the sender of the message
//            return VIEW_TYPE_MESSAGE_SENT;
//
//        } else {
//            return VIEW_TYPE_MESSAGE_RECEIVED;
//        }

        return position;
    }

    public class FileDownloader extends AsyncTask<Void,Void,Boolean>{
        File file;
        QBAttachment attachment;
        InputStream inputStream;
        ReceivedMessageHolder holder;
        SentMessageHolder sentMessageHolder;
        String identify;

        public FileDownloader(File file, QBAttachment attachment, InputStream inputStream, ReceivedMessageHolder holder, String identify) {
            this.file = file;
            this.attachment = attachment;
            this.inputStream = inputStream;
            this.holder = holder;
            this.identify = identify;
        }

        public FileDownloader(File file, QBAttachment attachment, InputStream inputStream, String identify, SentMessageHolder sentMessageHolder) {
            this.file = file;
            this.attachment = attachment;
            this.inputStream = inputStream;
            this.sentMessageHolder = sentMessageHolder;
            this.identify = identify;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }


        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            Log.d("Here","onPost");
            if(aBoolean){
                if(identify.equals("sent")){
                    sentMessageHolder.fileProcessingSent(file,attachment.getType(),attachment.getName());
                }
                else  if(identify.equals("receive")){
                    Log.d("Here","Here");
                    holder.fileProcessingReceived(file,attachment.getType(),attachment.getName());
                }
            }
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            Log.d("Here","Start");
            File file=new File(Constants.fullpath+
                    attachment.getName());

            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                OutputStream fos = new FileOutputStream(file);
                //com.quickblox.core.io.IOUtils.copy(inputStream,fos);
                byte[] buffer = new byte[4096*2];//Set buffer type
//            int len1 = 0;//init length
//            while ((len1 = inputStream.read(buffer)) != -1) {
//                fos.write(buffer, 0, len1);//Write new file
//            }
//
//            //Close all connection after doing task
//            fos.close();
                //inputStream.close();
                long fileSizeDownloaded=0;
                while (true) {
                    int read = inputStream.read(buffer);

                    if (read == -1) {
                        break;
                    }

                    fos.write(buffer, 0, read);

                    fileSizeDownloaded += read;

                }
                fos.flush();
                fos.close();
                inputStream.close();
                return true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
    }
}
