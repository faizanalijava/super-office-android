package com.supertec.superofficeapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.activities.rehanActivities.EmployeeCurrentLocationActivity;
import com.supertec.superofficeapp.activities.rehanActivities.TaskAssign;
import com.supertec.superofficeapp.models.EmployeeModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Programmer on 1/16/2018.
 */

public class Employee_List_Adapter extends RecyclerView.Adapter<Employee_List_Adapter.MyViewHolder> {
    Context mContext;
    List<EmployeeModel> list;

    public Employee_List_Adapter(Context mContext) {
        this.mContext = mContext;
        list=new ArrayList<>();
    }

    public void setList(List<EmployeeModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }
    public void changeData(String id,String status){

        for(int i=0;i<list.size();i++){
            if(list.get(i).getId().equals(id)){
                EmployeeModel model=list.get(i);
                model.setmEmployeeCurrentState(status);
                list.remove(i);
                switch (status) {
                    case "office":
                        list.add(0,model);
                        notifyItemChanged(i);
                        notifyItemMoved(i,0);
                        break;
                    case "outside":
                        list.add(list.size(),model);
                        notifyItemChanged(i);
                        notifyItemMoved(i,list.size()-1);
                        break;
                    case "home":
                        list.add(list.size(),model);
                        notifyItemChanged(i);
                        notifyItemMoved(i,list.size()-1);
                        break;
                }

                //notifyDataSetChanged();
                return;
            }
        }


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(
                R.layout.employee_item,parent,false
        );
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if(list.get(position).getmEmployeeCurrentState().equals("outside")){
            holder.txt_employee.setTextColor(ContextCompat.getColor(mContext,R.color.red));
            holder.employee_img.setBorderColor(ContextCompat.getColor(mContext,R.color.red));
        }
        else if(list.get(position).getmEmployeeCurrentState().equals("office")){
            holder.txt_employee.setTextColor(ContextCompat.getColor(mContext,R.color.green));
            holder.employee_img.setBorderColor(ContextCompat.getColor(mContext,R.color.green));
        }
        else if(list.get(position).getmEmployeeCurrentState().equals("home")){
            holder.txt_employee.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
            holder.employee_img.setBorderColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }
        holder.txt_employee.setText(list.get(position).getmEmployeeName());
        Glide.with(App.getContext()).load(Constants.getInstance().base_url+list.get(position).getmEmployeeImageUrl())
                .into(holder.employee_img);
        //Picasso.with(mContext).load(Constants.getInstance().base_url+list.get(position).getmEmployeeImageUrl()).into(holder.employee_img);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, EmployeeCurrentLocationActivity.class);
                intent.putExtra("user",(Serializable)list.get(position));
                if(Build.VERSION.SDK_INT<=23){
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                mContext.startActivity(intent);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                //Toast.makeText(mContext, ""+position, Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(mContext, TaskAssign.class);
                intent.putExtra("user",(Serializable)list.get(position));
                if(Build.VERSION.SDK_INT<=23){
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                }
                mContext.startActivity(intent);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        CircleImageView employee_img;
        TextView txt_employee;

        public MyViewHolder(View itemView) {
            super(itemView);
            employee_img=(CircleImageView) itemView.findViewById(R.id.employee_image);
            txt_employee=(TextView) itemView.findViewById(R.id.txt_employee_name);
        }
    }
}
