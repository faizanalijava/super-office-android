package com.supertec.superofficeapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogCustomData;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.activities.ChatActivity;
import com.supertec.superofficeapp.models.EmployeeModel;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Programmer on 2/6/2018.
 */

public class CreateNewDialogAdapter extends RecyclerView.Adapter<CreateNewDialogAdapter.CreateNewDialogHolder> {
    int size=0;
    List<EmployeeModel> list;
    Context context;

    public CreateNewDialogAdapter(Context context) {
        this.context = context;
    }

    public void setList(List<EmployeeModel> list) {
        this.list = list;
        size=list.size();
        notifyDataSetChanged();
    }

    @Override
    public CreateNewDialogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(
                R.layout.list_create_new_dialog_item,parent,false
        );
        return new CreateNewDialogHolder(view);
    }

    @Override
    public void onBindViewHolder(CreateNewDialogHolder holder, final int position) {
        Glide.with(App.getContext()).load(Constants.base_url+list.get(position).getmEmployeeImageUrl())
                .into(holder.img);
        holder.txt_name.setText(list.get(position).getmEmployeeName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog(position);
            }
        });
    }
    private void createDialog(int position){
        int participantId = Integer.parseInt(list.get(position).getQb_id());
        ArrayList occupantIdsList = new ArrayList();
        occupantIdsList.add(participantId);

        QBChatDialog dialog = new QBChatDialog();
        dialog.setType(QBDialogType.PRIVATE);
        dialog.setName(list.get(position).getmEmployeeName());
        dialog.setPhoto(list.get(position).getmEmployeeImageUrl());
        dialog.setOccupantsIds(occupantIdsList);
        QBDialogCustomData data=new QBDialogCustomData("custom");
        data.putString("owner_image",App.getInstance().getPref().getImage_Url());
        dialog.setCustomData(data);
        //or just use DialogUtils for creating PRIVATE dialog
        //QBChatDialog dialog = DialogUtils.buildPrivateDialog(recipientId);

        QBRestChatService.createChatDialog(dialog).performAsync(new QBEntityCallback() {
            @Override
            public void onSuccess(Object o, Bundle bundle) {
                QBChatDialog qbChatDialog=(QBChatDialog) o;
                if(QBChatService.getInstance().isLoggedIn()) {
                    Intent intent = new Intent(App.getContext(), ChatActivity.class);
                    intent.putExtra("EXTRA_DIALOG", (QBChatDialog)o);
                    intent.putExtra("check", "fromdialog");
                    if(Build.VERSION.SDK_INT<=23){
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    App.getContext().startActivity(intent);
                    ((Activity)context).finish();
                }
                //Toast.makeText(App.getContext(),"Conversation Created", Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onError(QBResponseException responseException) {

            }
        });

    }


    @Override
    public int getItemCount() {
        return size;
    }

    public static class CreateNewDialogHolder extends RecyclerView.ViewHolder{
        CircleImageView img;
        TextView txt_name;
        public CreateNewDialogHolder(View itemView) {
            super(itemView);
            img=(CircleImageView) itemView.findViewById(R.id.user_image);
            txt_name=(TextView) itemView.findViewById(R.id.txt_name);
        }
    }
}
