package com.supertec.superofficeapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.activities.EmployeeActivities.Task_Detail;
import com.supertec.superofficeapp.activities.Task_detail_new;
import com.supertec.superofficeapp.activities.rehanActivities.Task_Detail_Rehan;
import com.supertec.superofficeapp.models.TaskModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Programmer on 1/17/2018.
 */

public class Employee_Task_Adapter extends RecyclerView.Adapter<Employee_Task_Adapter.MyViewHolder> {
    Context mContext;
    List<TaskModel> list;
    String user_type;

    public Employee_Task_Adapter(Context mContext,String user_type) {
        this.mContext = mContext;
        this.user_type=user_type;
        list=new ArrayList<>();
    }

    public void setList(List<TaskModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public Employee_Task_Adapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(
                R.layout.list_task_item,parent,false
        );
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Employee_Task_Adapter.MyViewHolder holder, final int position) {
        String[] split=list.get(position).getTask_date().split("T");

        holder.txt_task_status.setText(list.get(position).getTask_status());
        if(list.get(position).getTask_status().equals("in progress")){
            holder.txt_task_status.setTextColor(ContextCompat.getColor(mContext,R.color.red));
        }
        else if(list.get(position).getTask_status().equals("completed")){
            holder.txt_task_status.setTextColor(ContextCompat.getColor(mContext,R.color.green));
        }
        else if(list.get(position).getTask_status().equals("Approval request")){
            holder.txt_task_status.setTextColor(ContextCompat.getColor(mContext,R.color.colorPrimary));
        }
        holder.txt_task_name.setText(list.get(position).getTask_name());
        holder.txt_task_date.setText(split[0]);
        Picasso.with(mContext).load(Constants.getInstance().base_url+"task.png").into(
                holder.imageView
        );
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, ""+position, Toast.LENGTH_SHORT).show();
                if(user_type.equals("rehan")){
                    Intent intent=new Intent(mContext, Task_detail_new.class);
                    intent.putExtra("task", (Serializable)list.get(position));
                    if(Build.VERSION.SDK_INT<=23){
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    mContext.startActivity(intent);
                }
                else if(user_type.equals("employee")){
                    Intent intent=new Intent(mContext, Task_detail_new.class);
                    intent.putExtra("task", (Serializable)list.get(position));
                    if(Build.VERSION.SDK_INT<=23){
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    mContext.startActivity(intent);
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txt_task_name,txt_task_date,txt_task_status;
        ImageView imageView;
        public MyViewHolder(View itemView) {
            super(itemView);
            txt_task_name=(TextView) itemView.findViewById(R.id.txt_task_name);
            txt_task_date=(TextView) itemView.findViewById(R.id.txt_task_date);
            txt_task_status=(TextView) itemView.findViewById(R.id.txt_task_status);
            imageView=(ImageView) itemView.findViewById(R.id.task_image);
        }
    }
}
