package com.supertec.superofficeapp.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.quickblox.chat.QBChatService;
import com.quickblox.chat.extensions.connections.bosh.QBBoshChatConnectionFabric;
import com.quickblox.chat.extensions.connections.bosh.QBBoshConfigurationBuilder;

import org.jivesoftware.smack.SmackException;

/**
 * Created by Programmer on 2/7/2018.
 */

public class ChatConenctionService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        QBBoshConfigurationBuilder configurationBuilder = new QBBoshConfigurationBuilder()
                .setUseTls(false) //Sets the TLS security mode used when making the connection. By default TLS is disabled.
                .setPort(5281) //Sets chat connection port number. By default for BOSH connection sets 5281
                .setHost("chat.quickblox.com") //Sets connection's host. By default used chat endpoint of your app.
                .setServiceName("chat.quickblox.com") //Sets the service name of XMPP service (i.e., the XMPP domain). By default used chat endpoint of your app.
                 //Sets chat socket's read timeout in seconds
                .setUseHttps(true) //Sets using "https" for connection. By default is true.
                .setFile("/http-bind/") //Sets the file name. By default is "/http-bind/".
                .setAutoMarkDelivered(false) //Sets automatic mark received messages as delivered. By default for BOSH connection sets false.
                .setAutojoinEnabled(false); //Sets true to automatically join loaded or created on server dialogs. By default sets false.
        QBBoshChatConnectionFabric connectionFabric = new QBBoshChatConnectionFabric(configurationBuilder);
        QBChatService.setConnectionFabric(connectionFabric);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
