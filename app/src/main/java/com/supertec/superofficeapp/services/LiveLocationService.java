package com.supertec.superofficeapp.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.VolleyError;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.ConnectivityCheck;
import com.supertec.superofficeapp.ConstantClasses.IResult;
import com.supertec.superofficeapp.ConstantClasses.VolleyService;
import com.supertec.superofficeapp.SharedPreferences.NoInternetPref;
import com.supertec.superofficeapp.api.ApiRoutes;
import com.supertec.superofficeapp.api.ApiService;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Programmer on 3/14/2018.
 */

public class LiveLocationService extends Service {
    private static VolleyService mVolley;
    private static IResult mIResult;

    private LocationCallback mLocationCallback;
    private GoogleApiClient mGoogleApiClient;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 2000;
    private FusedLocationProviderClient mFusedLocationClient;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    private LocationRequest mLocationRequest;
    ApiRoutes updateLocation;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        updateLocation= ApiService.getInstance().RootService(ApiRoutes.class);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        callBack();
        mVolley=new VolleyService(mIResult,App.getContext());
        createLocationCallback();
        createLocationRequest();
        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                mLocationCallback, Looper.myLooper());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }
    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Location mLocation=locationResult.getLastLocation();
                if(mLocation!=null) {
                    //Log.d("Location Result", locationResult.getLastLocation().getLatitude() + "");
                    if(ConnectivityCheck.getInstance().isConnected(App.getContext())) {
                        updateEmployeeLocation(String.valueOf(mLocation.getLatitude()), String.valueOf(mLocation.getLongitude()));
                        if(App.getInstance().getPref().getUser_Type().equals("employee")){
                            float[] office_distance = new float[1];
                            Location.distanceBetween(App.getInstance().getPref().getOffice_lat()
                                    ,App.getInstance().getPref().getOffice_lng(),mLocation.getLatitude()
                                    ,mLocation.getLongitude(),office_distance);
                            float[] home_distance = new float[1];
                            Location.distanceBetween(App.getInstance().getPref().getHome_lat()
                                    ,App.getInstance().getPref().getHome_lng(),mLocation.getLatitude()
                                    ,mLocation.getLongitude(),home_distance);
                            if(office_distance[0]<=25){
                                sendRequest("office");
                            }
                            else if(home_distance[0]<=25){
                                sendRequest("home");
                            }
                            else if(home_distance[0]>=25&&office_distance[0]>=25){
                                sendRequest("outside");
                            }
                        }
                    }
                }
            }
        };
    }
    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
    private void updateEmployeeLocation(String lat,String lng){
        HashMap map=new HashMap();
        map.put("lat",lat);
        map.put("lng",lng);
        Call<ResponseBody> call=updateLocation.UpdateCurrentLocation(map);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if(response!=null) {
//                    try {
//                       // Log.d("Retrofit", response.body().string());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
               // Log.d("Retrofit",t.getMessage().toString());
            }
        });

    }
    private void sendRequest(String status){
        if(!App.getInstance().getPref().getLastStatus().equals(status)) {

            if (ConnectivityCheck.getInstance().isConnected(App.getContext())) {
                HashMap map = new HashMap();
                map.put("status", status);
                mVolley.postDataWithAuthorization("status",
                        "changestatus", map, App.getInstance().getPref().getToken());
            } else {
                NoInternetPref.getInstance().setRequest(status);
            }
        }
        App.getInstance().getPref().setLastStatus(status);
    }
    private void callBack(){
        mIResult=new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                //Log.d("mymy",response);
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {

            }

            @Override
            public void notifyNoInternet(String requestType, String url, HashMap map, String token) {

            }
        };
    }
}
