package com.supertec.superofficeapp.services;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.receivers.EmployeeLocationReceiver;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Programmer on 1/20/2018.
 */

public class EmployeeLocationMonitor extends Service {
    private static final String TAG = EmployeeLocationMonitor.class.getSimpleName();
    private GeofencingClient mGeofencingClient;
    List<Geofence> mGeo=new ArrayList<>();
    PendingIntent mGeofencePendingIntent=null;
    SharedPref pref;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        pref=new SharedPref(getApplicationContext());
        mGeofencingClient = LocationServices.getGeofencingClient(this);
        //AddGeo();
        AddGeoFences();
        Log.d(TAG,"Oncraete");

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"Destroy");
        mGeofencingClient.removeGeofences(mGeofencePendingIntent);
    }
//    private void startGoogleApiClient(){
//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .addApi(LocationServices.API)
//                .addConnectionCallbacks(connectionAddListener)
//                .addOnConnectionFailedListener(connectionFailedListener)
//                .build();
//        mGoogleApiClient.connect();
//    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }
    private void AddGeo(){
        //mGeo.add();
    }
    private GeofencingRequest getGeofencingRequest() {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(Geofence.GEOFENCE_TRANSITION_EXIT|Geofence.GEOFENCE_TRANSITION_ENTER);
        Geofence office_geoffence=new Geofence.Builder().setRequestId("office")
                .setCircularRegion(
                        pref.getOffice_lat(), pref.getOffice_lng(),
                        25.0f
                )
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER|Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();
        Geofence home_geoffence=new Geofence.Builder().setRequestId("home")
                .setCircularRegion(
                        pref.getHome_lat(), pref.getHome_lng(),
                        25.0f
                )
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                .build();
        mGeo.add(office_geoffence);
        mGeo.add(home_geoffence);
        builder.addGeofences(mGeo);
        return builder.build();
    }
    private PendingIntent getGeofencePendingIntent() {
        // Reuse the PendingIntent if we already have it.

        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        Intent intent = new Intent(this, EmployeeLocationReceiver.class);
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when
        // calling addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }
    private void AddGeoFences(){
        mGeofencingClient.addGeofences(getGeofencingRequest(), getGeofencePendingIntent())
                .addOnSuccessListener( new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG,"Success");
                    }
                })
                .addOnFailureListener( new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG,"Fail "+e.getCause());
                    }
                });

    }
}
