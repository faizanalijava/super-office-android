package com.supertec.superofficeapp.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBIncomingMessagesManager;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.ChatSetup;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.activities.ChatActivity;
import com.supertec.superofficeapp.activities.EmployeeActivities.EmployeeDashboard;
import com.supertec.superofficeapp.activities.rehanActivities.RehanDashboard;

import java.util.ArrayList;

/**
 * Created by Programmer on 2/20/2018.
 */

public class AllMessagesListener extends Service {
    QBIncomingMessagesManager incomingMessagesManager;
    QBChatDialogMessageListener messagesListener;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if(!Constants.isInitialize){
            ChatSetup.getInstance().InitializeChat();
        }
        incomingMessagesManager= QBChatService.getInstance().getIncomingMessagesManager();
        messagesListener=new QBChatDialogMessageListener() {
            @Override
            public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {
                sendBroadcast(new Intent("com.supertec.superofficeapp.newmsg"));
                if(Constants.isInBackground){
                   GenerateNotification(qbChatMessage);
                }

            }

            @Override
            public void processError(String s, QBChatException e, QBChatMessage qbChatMessage, Integer integer) {

            }
        };
        if(incomingMessagesManager!=null) {
            incomingMessagesManager.addDialogMessageListener(messagesListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            if(incomingMessagesManager!=null) {
                incomingMessagesManager.removeDialogMessageListrener(messagesListener);
            }
        }
        catch (Exception e){}
    }
    private void GenerateNotification(QBChatMessage message){
        String CHANNEL_ID = message.getId().toString();
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher);
// Creates an explicit intent for an Activity in your app
        Intent resultIntent =null;
        if(App.getInstance().getPref().getUser_Type().equals("rehan")) {
            resultIntent = new Intent(this, RehanDashboard.class);
        }
        else{
            resultIntent = new Intent(this, EmployeeDashboard.class);
        }
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from    the Activity leads out of
// your app to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mBuilder.setContentText(message.getBody()).setContentTitle(message.getProperty("name").toString())
                //.setFullScreenIntent(resultPendingIntent, true)
                .setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle())
                .setAutoCancel(true);
        mNotificationManager.notify(1, mBuilder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }
}
