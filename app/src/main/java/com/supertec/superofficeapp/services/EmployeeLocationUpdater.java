package com.supertec.superofficeapp.services;

import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.supertec.superofficeapp.ConstantClasses.RunTimePermissions;
import com.supertec.superofficeapp.api.ApiRoutes;
import com.supertec.superofficeapp.api.ApiService;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Programmer on 3/8/2018.
 */

public class EmployeeLocationUpdater extends JobService {
    private FusedLocationProviderClient mFusedLocationClient;
    @Override
    public boolean onStartJob(JobParameters job) {
        Log.d("Trigger","Job");
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if(RunTimePermissions.runtimepermission()) {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            Log.d("Locationoutside", "");
                            if (location != null) {
                                Log.d("Location", location.getLatitude() + "  " + location.getLongitude());
                                updateEmployeeLocation(String.valueOf(location.getLatitude()), String
                                        .valueOf(location.getLongitude()));

                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d("failure", "");
                }
            });
        }


        return false;
    }

    @Override
    public boolean onStopJob(JobParameters job) {
        return false;
    }
    private void updateEmployeeLocation(String lat,String lng){
        HashMap map=new HashMap();
        map.put("lat",lat);
        map.put("lng",lng);
        ApiRoutes updateRehan= ApiService.getInstance().RootService(ApiRoutes.class);
        Call<ResponseBody> call=updateRehan.UpdateCurrentLocation(map);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.body()!=null) {
                    try {
                        Log.d("Retrofit", response.body().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //Log.d("Retrofit",t.getMessage().toString());
            }
        });

    }
}

