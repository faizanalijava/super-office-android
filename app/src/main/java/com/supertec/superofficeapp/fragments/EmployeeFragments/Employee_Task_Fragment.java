package com.supertec.superofficeapp.fragments.EmployeeFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.IResult;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.ConstantClasses.VolleyService;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.adapters.Employee_Task_Adapter;
import com.supertec.superofficeapp.models.TaskModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Employee_Task_Fragment extends Fragment {
    RecyclerView mTaskList;
    SharedPref pref;
    IResult mIResult;
    VolleyService mVolleyService;
    View mView;
    Gson gson;
    String RequestTag="get tasks";
    Employee_Task_Adapter adapter;

    public Employee_Task_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView=inflater.inflate(R.layout.employee_task_fragement, container, false);
        variableInitialization();
        return mView;
    }
    private void variableInitialization(){

         gson=new GsonBuilder().create();
        pref=new SharedPref(getContext());
        callBack();
        mVolleyService=new VolleyService(mIResult,getContext());
        mTaskList=(RecyclerView) mView.findViewById(R.id.task_list);
        mTaskList.setHasFixedSize(true);
        mTaskList.setLayoutManager(new LinearLayoutManager(getContext()));
        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), resId);
        mTaskList.setLayoutAnimation(animation);
        adapter=new Employee_Task_Adapter(getContext(),pref.getUser_Type());
        mTaskList.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(3000);
//                    getTasks();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();
        getTasks();

    }

    private void getTasks(){
        HashMap map=new HashMap();
        map.put("user_id",pref.getID());
        mVolleyService.postDataWithAuthorization(RequestTag,"gettasks",map,
                pref.getToken());
    }
    private void callBack(){
        mIResult=new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                //Toast.makeText(getContext(), response, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject parentObj=new JSONObject(response);
                    JSONObject responseObj=parentObj.getJSONObject("response");
                    if(responseObj.getInt("status")==200){
                        JSONObject resultObj=parentObj.getJSONObject("result");
                        List<TaskModel> tasks= Arrays.asList(gson.fromJson(
                                resultObj.getJSONArray("tasks").toString(),TaskModel[].class
                        ));
                        if(tasks.size()>0) {
                            adapter.setList(tasks);
                        }
                        else{
                            Toast.makeText(App.getContext(), "No Task Found", Toast.LENGTH_SHORT).show();
                        }

                    }
                    else{
                        Toast.makeText(App.getContext(),responseObj.getInt("status")+" "+responseObj
                                .getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {

            }
            @Override
            public void notifyNoInternet(String requestType, String url, HashMap map, String token) {

            }
        };
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
