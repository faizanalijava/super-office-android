package com.supertec.superofficeapp.fragments.EmployeeFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.IResult;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.ConstantClasses.VolleyService;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.activities.LoginActivity;
import com.supertec.superofficeapp.models.TaskModel;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class Add_Update_Fragment extends Fragment {
    private CircleImageView imageView;
    private TextView txt_task_name;
    private MaterialEditText input_update_description;
    private Button btn_add_updates;
    private View mView;
    private SharedPref pref;
    private IResult mIResult;
    private VolleyService mVolleyService;
    private Bundle bundle;
    private TaskModel model;

    public Add_Update_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        bundle=getArguments();
        model=(TaskModel) bundle.getSerializable("task");
        mView=inflater.inflate(R.layout.add_update_fragment, container, false);
        callBack();
        variableInitialization();
        return mView;
    }
    private void variableInitialization(){
        pref=new SharedPref(getContext());
        txt_task_name=(TextView) mView.findViewById(R.id.txt_task_name);
        input_update_description=(MaterialEditText)mView.findViewById(R.id.input_update_description);
        btn_add_updates=(Button) mView.findViewById(R.id.btn_give_updates);
        mVolleyService=new VolleyService(mIResult,getContext());
        imageView=(CircleImageView) mView.findViewById(R.id.circleImageView);
        txt_task_name.setText(model.getTask_name());
        btn_add_updates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(input_update_description.getText().length()>0){
                    sendUpdates();
                }
                else{
                    new LovelyInfoDialog(getContext())
                            .setTopColorRes(R.color.colorPrimary)
                            .setIcon(R.drawable.ic_alert)
                            //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                            .setTitle("Update Failed")
                            .setMessage("Update Description can not be empty")
                            .show();
                }
            }
        });
    }
    private void callBack(){
        mIResult=new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                try {
                    JSONObject parentObj=new JSONObject(response);
                    JSONObject responseObj=parentObj.getJSONObject("response");
                    if(responseObj.getInt("status")==200){
                        Toast.makeText(App.getContext(), "Operation Successfull", Toast.LENGTH_SHORT).show();
                        input_update_description.setText("");
                    }
                    else{
                        Toast.makeText(App.getContext(), responseObj.getInt("status")+"  "+
                                responseObj.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {

            }
            @Override
            public void notifyNoInternet(String requestType, String url, HashMap map, String token) {

            }
        };
    }

    private void sendUpdates(){
        HashMap map=new HashMap();
        map.put("task_id",model.getTask_id());
        map.put("update_description",input_update_description.getText().toString());
        mVolleyService.postDataWithAuthorization("add updates",
                "giveupdates",map,pref.getToken());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        imageView.setImageDrawable(null);
    }
}
