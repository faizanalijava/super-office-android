package com.supertec.superofficeapp.fragments.RehanFragments;


import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.IResult;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.ConstantClasses.VolleyService;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.adapters.Employee_List_Adapter;
import com.supertec.superofficeapp.models.EmployeeModel;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * A simple {@link Fragment} subclass.
 */
public class Rehan_Dashboard_Fragment extends Fragment {
    View mView;
    RecyclerView mEmployeeList;
    private IResult mIResult;
    private SharedPref pref;
    VolleyService mVolleyService;
    Employee_List_Adapter adapter;
    Socket mSocket;

    public Rehan_Dashboard_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView=inflater.inflate(R.layout.rehan_dashboard_fragment, container, false);
        variableInitialization();

        return mView;
    }
    public void variableInitialization(){

        mEmployeeList=(RecyclerView)mView.findViewById(R.id.employee_list);
        mEmployeeList.setHasFixedSize(true);
        mEmployeeList.setLayoutManager(new GridLayoutManager
                (getContext(),calculateNoOfColumns(getContext())));
        callBack();
        mVolleyService=new VolleyService(mIResult,getContext());
        adapter=new Employee_List_Adapter(getActivity().getApplicationContext());
        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), resId);
        mEmployeeList.setLayoutAnimation(animation);
        mEmployeeList.setAdapter(adapter);
        pref=new SharedPref(getContext());
        sendRequest();
        mSocket=App.getInstance().getSocket();
        mSocket.on(Socket.EVENT_CONNECT,OnConnect);
        mSocket.on("updates",updates);
        mSocket.connect();

    }
    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 110);
        return noOfColumns;
    }
    public void callBack(){
        mIResult=new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {

                try {
                    JSONObject mParentObj=new JSONObject(response);
                    JSONObject mResponseObj=mParentObj.getJSONObject("response");
                    if(mResponseObj.getInt("status")==200){
                        Gson gson=new GsonBuilder().create();
                        JSONObject mResultObj=mParentObj.getJSONObject("result");
                        String users=mResultObj.getJSONArray("users").toString();
                        Log.d("users",users);
                        List<EmployeeModel> employees =new ArrayList<>( Arrays.asList(gson.fromJson(users
                                , EmployeeModel[].class)));
//                        for (EmployeeModel model:
//                             employees) {
//                            Log.d("entries",""+model.getmEmployeeemail()+model.getmEmployeeImageUrl());
//                        }
                        if(employees.size()>0){

                            adapter.setList(employees);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {

            }
            @Override
            public void notifyNoInternet(final String requestType, final String url, final HashMap map, final String token) {
                new LovelyStandardDialog(getContext())
                        .setTopColorRes(R.color.colorPrimary)
                        .setButtonsColorRes(R.color.colorPrimaryDark)
                        .setIcon(R.drawable.ic_alert)
                        .setTitle("No Internet Found")
                        .setMessage("It looks like your internet is off please connect to " +
                                "internet and press retry button Thanks")
                        .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mVolleyService.postDataWithAuthorization(requestType,
                                        url,map,token);
                            }
                        }).setCancelable(false)
                        .show();
            }
        };
    }
    public void sendRequest(){
        HashMap<String,String> map=new HashMap<>();
        mVolleyService.postDataWithAuthorization("getAllEmployees","get",map,pref.getToken());
    }
    private Emitter.Listener OnConnect =new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if(getActivity()!=null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                      //  Toast.makeText(App.getContext().getApplicationContext(), "Connected", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    };
    private Emitter.Listener updates=new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if(getActivity()!=null){
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        Log.d("Sockets",data.toString());
                        try {
                            adapter.changeData(data.getString("user_id"),
                                   data.getString("status") );
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mSocket.connected()){
            mSocket.disconnect();
            mSocket.on("updates",updates);
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity){
            //this.listener = (MainActivity) context;
        }
    }
    private void just(String id,String status){

    }
}
