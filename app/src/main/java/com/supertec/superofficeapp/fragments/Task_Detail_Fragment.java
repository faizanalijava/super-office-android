package com.supertec.superofficeapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.models.TaskModel;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class Task_Detail_Fragment extends Fragment {
    CircleImageView imageView;
    private Bundle bundle;
    private TaskModel model;
    private TextView txt_task_name,txt_task_date,txt_task_description;
    View mView;
    public Task_Detail_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        bundle=getArguments();
        model=(TaskModel) bundle.getSerializable("task");
        mView=inflater.inflate(R.layout.task_detail_fragment, container, false);
        variableInitialization();
        return mView;
    }
    private void variableInitialization(){
        txt_task_name=(TextView) mView.findViewById(R.id.txt_task_name);
        txt_task_date=(TextView) mView.findViewById(R.id.txt_task_date);
        txt_task_description=(TextView) mView.findViewById(R.id.txt_task_description);
        imageView=(CircleImageView) mView.findViewById(R.id.circleImageView);
        txt_task_name.setText(model.getTask_name());
        String[] date=model.getTask_date().split("T");
        txt_task_date.setText(date[0]);
        txt_task_description.setText(model.getTask_Description());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        imageView.setImageDrawable(null);
    }
}
