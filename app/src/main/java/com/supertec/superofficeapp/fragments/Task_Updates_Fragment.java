package com.supertec.superofficeapp.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.IResult;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.ConstantClasses.VolleyService;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.adapters.Employee_Task_Adapter;
import com.supertec.superofficeapp.adapters.Task_update_Adapter;
import com.supertec.superofficeapp.models.TaskModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class Task_Updates_Fragment extends Fragment {
    private Bundle bundle;
    private TaskModel model;
    private View mView;
    private RecyclerView mUpdatesList;
    private Task_update_Adapter adapter;
    private SharedPref pref;
    private IResult mIResult;
    private VolleyService mVolleyService;
    private Gson gson;
    private boolean isOncreateCalled=false;
    private boolean isSetVisibilityCalled=false;

    public Task_Updates_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        isOncreateCalled=true;
        bundle=getArguments();
        model=(TaskModel) bundle.getSerializable("task");
        mView= inflater.inflate(R.layout.task_updates_fragment, container, false);
        callBack();
        variableInitialization();
        return mView;
    }
    private void variableInitialization(){
        gson=new GsonBuilder().create();
        pref=new SharedPref(App.getInstance().getApplicationContext());
        mVolleyService=new VolleyService(mIResult, App.getInstance().getApplicationContext());
        mUpdatesList=(RecyclerView) mView.findViewById(R.id.task_updates);
        mUpdatesList.setHasFixedSize(true);
        mUpdatesList.setLayoutManager(new LinearLayoutManager(App.getInstance().getApplicationContext()));
        adapter=new Task_update_Adapter();
        mUpdatesList.setAdapter(adapter);
        if(!isSetVisibilityCalled){
            Log.d("From","onCreate");
            getUpdates();
        }
    }
    private void callBack(){
        mIResult=new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                try {
                    JSONObject parentObj=new JSONObject(response);
                    JSONObject responseObj=parentObj.getJSONObject("response");
                    if(responseObj.getInt("status")==200){
                        JSONObject resultObj=parentObj.getJSONObject("result");
                        List<TaskModel.TaskUpdates> updates= Arrays.asList(gson.fromJson(
                                resultObj.getJSONArray("updates").toString(),TaskModel.TaskUpdates[].class
                        ));
                        //Toast.makeText(getContext(), ""+updates.size(), Toast.LENGTH_SHORT).show();
                        if(updates.size()>0){
                            adapter.setUpdates(updates);
                        }
                        else{
                            //Toast.makeText(getContext(), "no Updates Found for this task", Toast.LENGTH_SHORT).show();
                        }


                    }
                    else{
                        Toast.makeText(App.getInstance().getApplicationContext(),responseObj.getInt("status")+" "+responseObj
                                .getString("message"), Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {

            }
            @Override
            public void notifyNoInternet(String requestType, String url, HashMap map, String token) {

            }
        };
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            if(isOncreateCalled){
                isSetVisibilityCalled=true;
                Log.d("From","Visibilty");
                getUpdates();
            }

        }
    }
    private void getUpdates(){
        HashMap map=new HashMap();
        map.put("task_id",model.getTask_id());
        mVolleyService.postDataWithAuthorization("get Updates",
                "gettaskupdates",map,pref.getToken());
    }

    @Override
    public void onDestroy() {
        //mVolleyService.cancelAll();
        super.onDestroy();
        Log.d("ondestroy","upate");
    }
}
