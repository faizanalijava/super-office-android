package com.supertec.superofficeapp.fragments.EmployeeFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.supertec.superofficeapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Employee_Dashboard_Fragment extends Fragment {


    public Employee_Dashboard_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.employee_dashboard_fragment, container, false);
    }

}
