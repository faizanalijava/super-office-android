package com.supertec.superofficeapp.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBDialogCustomData;
import com.quickblox.chat.model.QBDialogType;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.model.QBUser;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.ChatSetup;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.activities.CreateNewDialog;
import com.supertec.superofficeapp.adapters.AllDialogAdapter;
import com.supertec.superofficeapp.api.ApiRoutes;
import com.supertec.superofficeapp.api.ApiService;
import com.supertec.superofficeapp.models.EmployeeModel;
import com.supertec.superofficeapp.services.ChatConenctionService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessagesFragment extends Fragment {
    private View mView;
    private FloatingActionButton btn_add;
    private RecyclerView mDialogs;
    private AllDialogAdapter adapter;
    private BroadcastReceiver broadcastReceiver;
    public MessagesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView= inflater.inflate(R.layout.messages_fragment, container, false);
        variableInitialization();
        //Toast.makeText(App.getContext(), ""+ Constants.isInitialize, Toast.LENGTH_SHORT).show();
        return mView;
    }
    private void variableInitialization(){
        initReceiver();
        //Toast.makeText(App.getContext(), ""+QBChatService.getInstance().isLoggedIn(), Toast.LENGTH_SHORT).show();
        if(!QBChatService.getInstance().isLoggedIn()){
            ChatSetup.getInstance().InitializeChat();
            loginToChat();
        }
        adapter=new AllDialogAdapter(getActivity());
        btn_add=(FloatingActionButton) mView.findViewById(R.id.addnewdialog);
        mDialogs=(RecyclerView) mView.findViewById(R.id.rc_dialogs);
        mDialogs.setHasFixedSize(true);
        mDialogs.setLayoutManager(new LinearLayoutManager(App.getContext()));
        mDialogs.setAdapter(adapter);
        btn_add.bringToFront();
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(App.getContext(), "You Tapped", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(App.getContext(), CreateNewDialog.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
        Constants.isInBackground=true;
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("Testing","stop");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("Testing","start");
    }

    @Override
    public void onResume() {
        super.onResume();
        Constants.isInBackground=false;
        Constants.currentDialog="";
        Log.d("Testing","resume");
        getDialogs();
    }
    private void getDialogs(){
        QBRestChatService.getChatDialogs(null, App.getInstance().getBuilder())
                .performAsync(new QBEntityCallback<ArrayList<QBChatDialog>>() {
                    @Override
                    public void onSuccess(ArrayList<QBChatDialog> qbChatDialogs, Bundle bundle) {
                        adapter.setList(qbChatDialogs);
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        Log.d("Error",e.getMessage().toString());
                    }
                });
    }

    private void createDialog(EmployeeModel rehan){
        int participantId = Integer.parseInt(rehan.getQb_id());
        ArrayList occupantIdsList = new ArrayList();
        occupantIdsList.add(participantId);

        QBChatDialog dialog = new QBChatDialog();
        dialog.setType(QBDialogType.PRIVATE);
        dialog.setName(rehan.getmEmployeeName());
        dialog.setPhoto(rehan.getmEmployeeImageUrl());
        dialog.setOccupantsIds(occupantIdsList);
        QBDialogCustomData data=new QBDialogCustomData("custom");
        data.putString("owner_image",App.getInstance().getPref().getImage_Url());
        dialog.setCustomData(data);
        //or just use DialogUtils for creating PRIVATE dialog
        //QBChatDialog dialog = DialogUtils.buildPrivateDialog(recipientId);


        QBRestChatService.createChatDialog(dialog).performAsync(new QBEntityCallback() {
            @Override
            public void onSuccess(Object o, Bundle bundle) {
                btn_add.setVisibility(View.GONE);
                getDialogs();
            }


            @Override
            public void onError(QBResponseException responseException) {

            }
        });
    }
    private void loginToChat(){
        if(App.getInstance().getPref().getToken()!=null) {
            QBUser user = new QBUser();
            user.setLogin(App.getInstance().getPref().getEmail());
            user.setPassword(App.getInstance().getPref().getPass());
            user.setId(Integer.parseInt(App.getInstance().getPref().getQbid()));
            QBChatService.getInstance().login(user, new QBEntityCallback() {
                @Override
                public void onSuccess(Object o, Bundle bundle) {
                    Log.d("Major","Success");
                }

                @Override
                public void onError(QBResponseException errors) {
                    Log.w("Major1",errors.getMessage()+errors.getStackTrace()+errors.getCause());
                }
            });
        }
    }
    private void initReceiver(){
        broadcastReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getDialogs();
            }
        };
        IntentFilter filter=new IntentFilter("com.supertec.superofficeapp.newmsg");
        App.getInstance().registerReceiver(broadcastReceiver,filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try{
            App.getInstance().unregisterReceiver(broadcastReceiver);
        }
        catch (Exception e){}
    }

}
