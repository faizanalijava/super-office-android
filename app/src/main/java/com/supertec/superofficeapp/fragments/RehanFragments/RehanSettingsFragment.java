package com.supertec.superofficeapp.fragments.RehanFragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.SharedPreferences.SettingsPref;
import com.supertec.superofficeapp.activities.UpdatePasswordActivity;
import com.supertec.superofficeapp.services.LiveLocationService;
import com.supertec.superofficeapp.services.RehanLocationService;

/**
 * Created by Programmer on 1/25/2018.
 */

public class RehanSettingsFragment extends Fragment {
    private Switch mLocationSharing;
    private View mView;
    private LinearLayout mChangePassword;
    public RehanSettingsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView=inflater.inflate(R.layout.rehan_settings_activity, container, false);
        variableInitialization();
        return mView;
    }
    private void variableInitialization(){
        mLocationSharing=(Switch) mView.findViewById(R.id.sw_location);
        mChangePassword=(LinearLayout)mView.findViewById(R.id.change_password);
        mChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(App.getContext(), UpdatePasswordActivity.class);
                startActivity(intent);
            }
        });
        handleSwitch();
    }
    private void handleSwitch(){
        mLocationSharing.setChecked(SettingsPref.getInstance().getLocationSharing());
        mLocationSharing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SettingsPref.getInstance().setLocationSharing(isChecked);
                processLocationSharing();
            }
        });
    }
    private void processLocationSharing(){
        Log.d("Job","sa");
        //FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(App.getContext()));
        if(SettingsPref.getInstance().getLocationSharing()){
            App.getContext().startService(new Intent(App.getContext(), LiveLocationService.class));
//            Job myJob = dispatcher.newJobBuilder()
//                    // the JobService that will be called
//                    .setService(RehanLocationService.class)
//                    // uniquely identifies the job
//                    .setTag("rehan-location")
//                    // one-off job
//                    .setRecurring(true)
//                    // don't persist past a device reboot
//                    .setLifetime(Lifetime.FOREVER)
//                    // start between 0 and 60 seconds from now
//                    .setTrigger(Trigger.executionWindow(0, 60))
//                    // don't overwrite an existing job with the same tag
//                    .setReplaceCurrent(false)
//                    // retry with exponential backoff
//                    .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
//                    // constraints that need to be satisfied for the job to run
//                    .setConstraints(
//                            // only run on an unmetered network
//                            Constraint.ON_UNMETERED_NETWORK
//                            // only run when the device is charging
//                    )
//                    .build();
//
//            dispatcher.mustSchedule(myJob);
            Toast.makeText(App.getContext(), "Location Sharing On", Toast.LENGTH_SHORT).show();
        }
        else{
            App.getContext().stopService(new Intent(App.getContext(), LiveLocationService.class));
            //dispatcher.cancelAll();
            Toast.makeText(App.getContext(), "Location Sharing Off", Toast.LENGTH_SHORT).show();
        }
    }
}
