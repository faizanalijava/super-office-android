package com.supertec.superofficeapp.fragments.EmployeeFragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.activities.EmployeeActivities.ChangeHomeLocation;
import com.supertec.superofficeapp.activities.UpdatePasswordActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmployeeSettingsFragment extends Fragment implements View.OnClickListener {
    private View mView;
    private LinearLayout mChangePassword;
    private LinearLayout mChangeHomeLocation;

    public EmployeeSettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView= inflater.inflate(R.layout.employee_settings_fragment, container, false);
        variableInitialization();
        return mView;
    }
    private void variableInitialization(){
        mChangePassword=(LinearLayout)mView.findViewById(R.id.change_password);
        mChangeHomeLocation=(LinearLayout)mView.findViewById(R.id.change_home_location);
        mChangePassword.setOnClickListener(this);
        mChangeHomeLocation.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.change_home_location:
                //Toast.makeText(App.getContext(), "You Click home", Toast.LENGTH_SHORT).show();
                Intent changeHomeLocation=new Intent(App.getContext(), ChangeHomeLocation.class);
                startActivity(changeHomeLocation);
                break;
            case R.id.change_password:
                //Toast.makeText(App.getContext(), "You Click password", Toast.LENGTH_SHORT).show();
                Intent changePassword=new Intent(App.getContext(), UpdatePasswordActivity.class);
                startActivity(changePassword);
                break;
        }
    }
}
