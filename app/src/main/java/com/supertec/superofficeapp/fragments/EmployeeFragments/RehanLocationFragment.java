package com.supertec.superofficeapp.fragments.EmployeeFragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.api.ApiRoutes;
import com.supertec.superofficeapp.api.ApiService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RehanLocationFragment extends Fragment {
    private CircleImageView rehan_image;
    private GoogleMap mMap;
    private MapView mMapView;
    private Marker mMarker = null;
    private View mView;
    OnMapReadyCallback onMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(App.getContext());
//        LatLng latLng=new LatLng(lat,longi);

            mMap = googleMap;
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.getUiSettings().setRotateGesturesEnabled(false);
            //addSingleMarker(new LatLng(31.5305032,74.3101452));
            getRehanLocation();

        }
    };

    public RehanLocationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView= inflater.inflate(R.layout.rehan_location_fragment, container, false);
        variableInitialization();
        return mView;
    }
    private void variableInitialization(){
        rehan_image=mView.findViewById(R.id.rehan_image);
        mMapView=(MapView) mView.findViewById(R.id.map);
        if(mMapView!=null){
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(onMapReadyCallback);
        }

    }
    private void addSingleMarker(LatLng latLng) {
        if (mMarker == null) {
            MarkerOptions options = new MarkerOptions().position(latLng).title("Rehan").icon(
                    BitmapDescriptorFactory.fromResource(R.drawable.ic_action_rehan_location)
            );

            mMarker = mMap.addMarker(options);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));

        }
    }
    private void getRehanLocation(){
        ApiRoutes getRehanLocationRoute= ApiService.getInstance().RootService(ApiRoutes.class);
        Call<ResponseBody> rehanLocationCall=getRehanLocationRoute.getRehanLocation();
        rehanLocationCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.body() !=null){
                    try {
                        JSONObject parentobj=new JSONObject(response.body().string());
                        JSONObject responseObj=parentobj.getJSONObject("response");
                        if(responseObj.getInt("status")==200){
                            callBackProcessing(parentobj.getJSONObject("result"));
                        }
                        else{
                            Toast.makeText(App.getContext(),responseObj.getInt("status") +" "+
                                    responseObj.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();


    }
    private void callBackProcessing(JSONObject objResult){
        try {
            String imageUrl=objResult.getString("rehan_image");
            Picasso.with(App.getContext()).load(Constants.base_url+imageUrl).networkPolicy(
                    NetworkPolicy.NO_CACHE
            ).into(rehan_image);
            JSONObject location=objResult.getJSONObject("location").getJSONObject("current_location");
            addSingleMarker(new LatLng(location.getDouble("current_lat"),location.getDouble("current_lng")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        mMapView.onDestroy();
        if(mMap!=null) {
            mMap.clear();
        }
        super.onDestroyView();
    }
}
