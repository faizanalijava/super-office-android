package com.supertec.superofficeapp.fragments.RehanFragments;


import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.IResult;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.ConstantClasses.VolleyService;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.adapters.Employee_List_Adapter;
import com.supertec.superofficeapp.adapters.Employee_Task_Adapter;
import com.supertec.superofficeapp.models.EmployeeModel;
import com.supertec.superofficeapp.models.TaskModel;
import com.wang.avi.AVLoadingIndicatorView;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class User_Tasks_Fragment extends Fragment {
    private Gson gson;
    private String currentUser="";
    private View mView;
    private MaterialSpinner mUsers;
    //AVLoadingIndicatorView loadingIndicatorView;
    private VolleyService mVolleyService;
    private IResult mIResult;
    private SharedPref pref;
    private RecyclerView mUserTask;
    private Employee_Task_Adapter adapter;
    public User_Tasks_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView=inflater.inflate(R.layout.just, container, false);
        callBack();
        variableInitialization();
      return mView;
    }
    private void variableInitialization(){
        gson= new GsonBuilder().create();
        mVolleyService=new VolleyService(mIResult,getContext());
        pref=new SharedPref(getContext());
        mUsers=(MaterialSpinner) mView.findViewById(R.id.UsersList);
        //loadingIndicatorView=(AVLoadingIndicatorView) mView.findViewById(R.id.loading);
//        List<String> list=new ArrayList<>();
//        list.add("A");
//        list.add("B");
//        list.add("C");
//        mUsers.setItems(list);
        mUserTask=(RecyclerView) mView.findViewById(R.id.user_tasks);
        mUserTask.setHasFixedSize(true);
        mUserTask.setLayoutManager(new LinearLayoutManager(getContext()));
        int resId = R.anim.layout_animation_fall_down;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(getContext(), resId);
        mUserTask.setLayoutAnimation(animation);
        adapter=new Employee_Task_Adapter(getActivity(),pref.getUser_Type());
        mUserTask.setAdapter(adapter);
//        loadingIndicatorView.setVisibility(View.VISIBLE);
//        loadingIndicatorView.show();
        getUsers();

    }
    private void getUsers(){
        HashMap<String,String> map=new HashMap<>();
        mVolleyService.postDataWithAuthorization("getAllEmployees","get",map,pref.getToken());
    }
    private void getUserTasks(String user_id){
        HashMap map=new HashMap();
        map.put("user_id",user_id);
        mVolleyService.postDataWithAuthorization("gettasks","gettasks",map,
                pref.getToken());
    }
    private void callBack(){
        mIResult=new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
//                loadingIndicatorView.setVisibility(View.INVISIBLE);
//                loadingIndicatorView.hide();
                mUserTask.setVisibility(View.VISIBLE);
                if(requestType.equals("gettasks")){
                    taskProcessing(response);
                }
                else if(requestType.equals("getAllEmployees")){
                    usersProcessing(response);
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {

            }

            @Override
            public void notifyNoInternet(final String requestType, final String url, final HashMap map, final String token) {
//                loadingIndicatorView.setVisibility(View.INVISIBLE);
//                loadingIndicatorView.hide();
                new LovelyStandardDialog(getContext())
                        .setTopColorRes(R.color.colorPrimary)
                        .setButtonsColorRes(R.color.colorPrimaryDark)
                        .setIcon(R.drawable.ic_alert)
                        .setTitle("No Internet Found")
                        .setMessage("It looks like your internet is off please connect to " +
                                "internet and press retry button Thanks")
                        .setPositiveButton(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                                loadingIndicatorView.setVisibility(View.VISIBLE);
//                                loadingIndicatorView.show();
                                mUserTask.setVisibility(View.INVISIBLE);
                                mVolleyService.postDataWithAuthorization(requestType,
                                        url,map,token);
                            }
                        }).setCancelable(false)
                        .show();
            }
        };
    }
    private void taskProcessing(String response){
        try {
            JSONObject parentObj=new JSONObject(response);
            JSONObject responseObj=parentObj.getJSONObject("response");
            if(responseObj.getInt("status")==200){
                JSONObject resultObj=parentObj.getJSONObject("result");
                List<TaskModel> tasks= Arrays.asList(gson.fromJson(
                        resultObj.getJSONArray("tasks").toString(),TaskModel[].class
                ));
                if(tasks.size()>0){
                    adapter.setList(tasks);
                }
                else{
                    adapter.setList(tasks);
                    Toast.makeText(App.getContext(), "No Tasks found", Toast.LENGTH_SHORT).show();
                }

            }
            else{
                Toast.makeText(App.getContext(),responseObj.getInt("status")+" "+responseObj
                        .getString("message"), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void usersProcessing(String response){
        try {
            JSONObject mParentObj=new JSONObject(response);
            JSONObject mResponseObj=mParentObj.getJSONObject("response");
            if(mResponseObj.getInt("status")==200){
                JSONObject mResultObj=mParentObj.getJSONObject("result");
                String users=mResultObj.getJSONArray("users").toString();
                Log.d("users",users);
                final List<EmployeeModel> employees = Arrays.asList(gson.fromJson(users
                        , EmployeeModel[].class));

                if(employees.size()>0){
                    List<String> usersName=new ArrayList<>();
                    for (int i=0;i<employees.size();i++){
                        usersName.add(employees.get(i).getmEmployeeName());
                    }
                    //Toast.makeText(getContext(), ""+employees.size()+"   "+usersName.size(), Toast.LENGTH_SHORT).show();
                    mUsers.setItems(usersName);
                    currentUser=employees.get(0).getId();
                    getUserTasks(employees.get(0).getId());
                    mUsers.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

                        @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
//                            loadingIndicatorView.setVisibility(View.VISIBLE);
//                            loadingIndicatorView.show();
                            mUserTask.setVisibility(View.INVISIBLE);
                            currentUser=employees.get(position).getId();
                            getUserTasks(employees.get(position).getId());
                        }
                    });

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!currentUser.equals("")){
            getUserTasks(currentUser);
        }
    }
}
