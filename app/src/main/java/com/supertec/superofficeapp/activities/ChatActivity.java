package com.supertec.superofficeapp.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.quickblox.auth.session.QBSessionManager;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBMessageStatusesManager;
import com.quickblox.chat.QBPingManager;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.exception.QBChatException;
import com.quickblox.chat.listeners.QBChatDialogMessageListener;
import com.quickblox.chat.listeners.QBChatDialogMessageSentListener;
import com.quickblox.chat.listeners.QBChatDialogTypingListener;
import com.quickblox.chat.listeners.QBMessageStatusListener;
import com.quickblox.chat.model.QBAttachment;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.chat.model.QBChatMessage;
import com.quickblox.content.QBContent;
import com.quickblox.content.model.QBFile;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.QBProgressCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.core.helper.StringifyArrayList;
import com.quickblox.messages.services.SubscribeService;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.ChatMessage;
import com.supertec.superofficeapp.ConstantClasses.ChatSetup;
import com.supertec.superofficeapp.ConstantClasses.ConnectivityCheck;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.ConstantClasses.PathUtil;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.activities.EmployeeActivities.EmployeeDashboard;
import com.supertec.superofficeapp.adapters.ChatAdapter;
import com.supertec.superofficeapp.services.ChatConenctionService;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.ping.PingFailedListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;

public class ChatActivity extends AppCompatActivity implements View.OnClickListener {
    private QBChatDialog qbChatDialog;
    private RecyclerView mChatMessages;
    private ChatAdapter adapter;
    private Button btn_send;
    private EditText edtMessage;
    private NotificationManager mNotifyManager;
    SharedPref pref;
    public int count=0;
    private int counter=1;
     QBPingManager pingManager;
    private BroadcastReceiver internetReceiver;
    public  int Pick_Image_From_Gallery_Request=1;
    public  int Pick_Documents_Request=2;
    public static int RQS_RECORDING = 3;
    private String status="";
    ArrayList<Integer> userids;
    private ImageView btn_back;
    private TextView txt_status,txt_name;
    private static final String Tag="ChatRealted";
    private int opponent_user=0;
    private QBMessageStatusesManager messageStatusesManager;
    private QBMessageStatusListener messageStatusListener;
    private QBChatDialogTypingListener typingListener;
    private QBChatDialogMessageListener messageListener;
    private PingFailedListener pingFailedListener;
    String filePath="";
    private ImageButton btn_Attach;
    private Handler mUserOnline=new Handler();
    private Runnable getUserOnline=new Runnable() {
        @Override
        public void run() {
            mUserOnline.postDelayed(this, 20000);
            checkUserOnline();

        }
    };
    private LinearLayout attachmentLayout;
    private boolean isHidden = true;
    String from="";
    String id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_activity);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder(); StrictMode.setVmPolicy(builder.build());
        if(!Constants.isInitialize) {
            ChatSetup.getInstance().InitializeChat();
        }
        if(savedInstanceState!=null){
            from="fromservice";
            id=App.getInstance().getPref().getDialogId();
        }
        else {
            from = getIntent().getExtras().getString("check");
            id = getIntent().getExtras().getString("id");
        }

        if(from.equals("fromservice")){
            if(checkSignIn()) {
                getDialog();
            }
            else{
                signIn();
            }
        }
        else{
            qbChatDialog = (QBChatDialog) getIntent().getSerializableExtra("EXTRA_DIALOG");
            variableInitialization();
        }
        InternetReceiver();
    }
    private void variableInitialization(){
        App.getInstance().getPref().setCurrentDialogid(qbChatDialog.getDialogId());
        Constants.currentDialog=qbChatDialog.getDialogId().toString();
        Constants.isInBackground=true;
        mNotifyManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        startService(new Intent(getApplicationContext(), ChatConenctionService.class));
        userids=new ArrayList<>();
        messageStatusesManager = QBChatService.getInstance().getMessageStatusesManager();
        txt_status=(TextView) findViewById(R.id.txt_status);
        txt_name=(TextView) findViewById(R.id.txt_name);
        btn_back=(ImageView) findViewById(R.id.btn_back);
        qbChatDialog.initForChat(QBChatService.getInstance());
        txt_name.setText(qbChatDialog.getName());
        pref=new SharedPref(App.getContext());
        btn_Attach=(ImageButton) findViewById(R.id.btn_attach);
        btn_send=(Button) findViewById(R.id.button_chatbox_send);
        edtMessage=(EditText) findViewById(R.id.edittext_chatbox);
        adapter=new ChatAdapter(pref.getQbid());
        mChatMessages=(RecyclerView) findViewById(R.id.reyclerview_message_list);
        mChatMessages.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        mChatMessages.setLayoutManager(linearLayoutManager);
        adapter.setHasStableIds(true);
        mChatMessages.setAdapter(adapter);
        List<QBChatMessage> temp=new ArrayList<>();
        adapter.setList(temp);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtMessage.getText().length()>0) {
                     QBChatMessage message=new QBChatMessage();
                    message.setBody(edtMessage.getText().toString());
                    message.setSaveToHistory(true);
                    message.setDialogId(qbChatDialog.getDialogId());
                    Date currentDate = new Date();
                    message.setProperty("name",pref.getName());
                    message.setProperty("image",pref.getImage_Url());
                    message.setDateSent(currentDate.getTime() / 1000);
                    message.setSenderId(Integer.parseInt(pref.getQbid()));
                    message.setRecipientId(opponent_user);
                    sendMessage(message);
                    edtMessage.setText("");

                }

            }
        });
        getAllMessages();
        addMessageListener();
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        List<Integer> users=qbChatDialog.getOccupants();
        for(int i=0;i<users.size();i++){
            if(!pref.getQbid().equals(String.valueOf(users.get(i)))){
                opponent_user=users.get(i);
            }
        }
        userids.add(opponent_user);
        addTypingListner();
        addTextWatcher();
        addMessageStateListener();
        txt_status.setText(status);
        attachmentLayout = (LinearLayout) findViewById(R.id.menu_attachments);

        ImageButton btnDocument = (ImageButton) findViewById(R.id.menu_attachment_document);
        ImageButton btnCamera = (ImageButton) findViewById(R.id.menu_attachment_camera);
        ImageButton btnGallery = (ImageButton) findViewById(R.id.menu_attachment_gallery);
//        ImageButton btnAudio = (ImageButton) findViewById(R.id.menu_attachment_audio);
//        ImageButton btnLocation = (ImageButton) findViewById(R.id.menu_attachment_location);
//        ImageButton btnContact = (ImageButton) findViewById(R.id.menu_attachment_contact);

        btnDocument.setOnClickListener(this);
        btnCamera.setOnClickListener(this);
        btnGallery.setOnClickListener(this);
        btn_Attach.setOnClickListener(this);
//        btnAudio.setOnClickListener(this);
//        btnLocation.setOnClickListener(this);
//        btnContact.setOnClickListener(this);
        //addPingManager();
        mUserOnline.post(getUserOnline);
        //checkUserOnline();
    }

    private void sendMessage(final QBChatMessage chatMessage) {
//        QBChatMessage fake=new QBChatMessage();
//        fake.setBody(text);
//        fake.setSaveToHistory(true);
//        String unique_id=
//        fake.setProperty("unique_id",getRandomString(5));
//        fake.setProperty("status","in process");
//        adapter.addNewMessage(fake);

//        QBRestChatService.createMessage(message,true).performAsync(new QBEntityCallback<QBChatMessage>() {
//            @Override
//            public void onSuccess(QBChatMessage message, Bundle bundle) {
//                getUnixTime();
//                message.setProperty("status","sent");
//                adapter.changeMessageStatus(message);
//                Log.w(Tag,"Sent");
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
//                Log.w(Tag,"Sent Failed");
//            }
//        });
        qbChatDialog.sendMessage(chatMessage, new QBEntityCallback<Void>() {
            @Override
            public void onSuccess(Void aVoid, Bundle bundle) {
                adapter.addNewMessage(chatMessage);
                mChatMessages.scrollToPosition(adapter.getItemCount()-1);
            }

            @Override
            public void onError(QBResponseException e) {
               // Toast.makeText(ChatActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getAllMessages(){
        QBRestChatService.getDialogMessages(qbChatDialog, App.getInstance().getMessageBuilder()
        ).performAsync(new QBEntityCallback<ArrayList<QBChatMessage>>() {
            @Override
            public void onSuccess(final ArrayList<QBChatMessage> qbChatMessages, Bundle bundle) {
                StringifyArrayList<String> messagesIDs =new StringifyArrayList<String>();
                for(int i=0;i<qbChatMessages.size();i++){

                    if(!qbChatMessages.get(i).getSenderId()
                            .toString().equals(pref.getQbid())&&!(qbChatMessages.get(i)
                    .getReadIds().toString().contains(pref.getQbid()))){
                        Log.d("Personal",String.valueOf(!qbChatMessages.get(i)
                                .getSenderId().toString().equals(pref.getQbid()))+" "+
                                String.valueOf(qbChatMessages.get(i)
                                        .getReadIds().toString().contains(pref.getQbid()))+" "
                                +qbChatMessages.get(i).getSenderId()+" "+pref.getQbid()
                                +" "+qbChatMessages.get(i).getRecipientId()+" "+messagesIDs.size());
                        messagesIDs.add(qbChatMessages.get(i).getId());
                }
                if(messagesIDs.size()>0){
                    Log.d("Personal",messagesIDs.size()+"");
                    QBRestChatService.markMessagesAsRead(qbChatDialog.getDialogId(),messagesIDs)
                            .performAsync(new QBEntityCallback<Void>() {
                                @Override
                                public void onSuccess(Void aVoid, Bundle bundle) {
                                    adapter.setList(qbChatMessages);
                                    mChatMessages.scrollToPosition(adapter.getItemCount()-1);
                                }

                                @Override
                                public void onError(QBResponseException e) {

                                }
                            });
                }

            }
                adapter.setList(qbChatMessages);
                mChatMessages.scrollToPosition(adapter.getItemCount()-1);

                Log.w(Tag,"Got"+qbChatMessages.size());
            }

            @Override
            public void onError(QBResponseException e) {
                Log.w(Tag,"GotFailed");
            }
        });
    }
    private void addMessageListener(){
        messageListener=new QBChatDialogMessageListener() {
            @Override
            public void processMessage(String s, QBChatMessage qbChatMessage, Integer integer) {
                qbChatDialog.readMessage(qbChatMessage, new QBEntityCallback() {
                    @Override
                    public void onSuccess(Object o, Bundle bundle) {
                        //Toast.makeText(ChatActivity.this, "Success", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(QBResponseException e) {
                        //Toast.makeText(ChatActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                adapter.addNewMessage(qbChatMessage);
                mChatMessages.scrollToPosition(adapter.getItemCount()-1);
                Log.w(Tag,"Addedd");
                //GenerateNotification(qbChatMessage.getBody());
            }

            @Override
            public void processError(String s, QBChatException e, QBChatMessage qbChatMessage, Integer integer) {
                Log.w(Tag,"NotAddedd");
            }
        };
        qbChatDialog.addMessageListener(messageListener);
    }

    private void addPingManager(){
        QBUsers.getUsersByIDs(userids,App.getInstance().PageBuilder()).performAsync(new QBEntityCallback<ArrayList<QBUser>>() {
            @Override
            public void onSuccess(ArrayList<QBUser> qbUsers, Bundle bundle) {
                long currentTime = System.currentTimeMillis();
                long userLastRequestAtTime = qbUsers.get(0).getLastRequestAt().getTime();

// if user didn't do anything last 5 minutes (5*60*1000 milliseconds)
                if((currentTime - userLastRequestAtTime) > 60*1000){
                    status="Offline";
                    txt_status.setText(status);
                }
                else{
                    status="Online";
                    txt_status.setText(status);
                }
            }

            @Override
            public void onError(QBResponseException e) {

            }
        });
//         pingManager= QBChatService.getInstance().getPingManager();
//        int userId = 56456;
//        Log.d("User",""+opponent_user);
//        pingFailedListener=new PingFailedListener() {
//            @Override
//            public void pingFailed() {
//                Toast.makeText(ChatActivity.this, "ping failed", Toast.LENGTH_SHORT).show();
//            }
//        };
//        pingManager.addPingFailedListener(pingFailedListener);
//        pingManager.pingUser(opponent_user, new QBEntityCallback<Void>() {
//            @Override
//            public void onSuccess(Void result, Bundle params) {
//                Toast.makeText(ChatActivity.this, ""+result, Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onError(QBResponseException e) {
//                Toast.makeText(ChatActivity.this, ""+e.getMessage()+e.getErrors(), Toast.LENGTH_SHORT).show();
//            }
//        });
////        pingManager.pingServer(new QBEntityCallback<Void>() {
////            @Override
////            public void onSuccess(Void aVoid, Bundle bundle) {
////                Toast.makeText(ChatActivity.this, "abc"+aVoid, Toast.LENGTH_SHORT).show();
////            }
////
////            @Override
////            public void onError(QBResponseException e) {
////                Toast.makeText(ChatActivity.this, "jhjh"+e.getErrors(), Toast.LENGTH_SHORT).show();
////            }
////        });

    }
    private void addTypingListner(){
        typingListener=new QBChatDialogTypingListener() {
            @Override
            public void processUserIsTyping(String s, Integer integer) {
                txt_status.setText("typing...");
            }

            @Override
            public void processUserStopTyping(String s, Integer integer) {
                txt_status.setText("Online");
            }
        };
        qbChatDialog.addIsTypingListener(typingListener);

    }
    boolean isTyping=false;
    final int TYPING_TIMEOUT = 5000; // 5 seconds timeout
    final Handler timeoutHandler = new Handler();
    private TextWatcher textWatcher;
    final Runnable typingTimeout = new Runnable() {
        public void run() {
            isTyping = false;
            try {
                qbChatDialog.sendStopTypingNotification();
            } catch (XMPPException e) {
                e.printStackTrace();
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
        }
    };
    private void addTextWatcher(){
        textWatcher=new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                timeoutHandler.removeCallbacks(typingTimeout);

                if (edtMessage.getText().toString().trim().length() > 0) {
                    // schedule the timeout
                    timeoutHandler.postDelayed(typingTimeout, TYPING_TIMEOUT);

                    if (!isTyping) {
                        isTyping = true;
                        try {
                            qbChatDialog.sendIsTypingNotification();
                        } catch (XMPPException e) {
                            e.printStackTrace();
                        } catch (SmackException.NotConnectedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else {
                    isTyping = false;
                    try {
                        qbChatDialog.sendStopTypingNotification();
                    } catch (XMPPException e) {
                        e.printStackTrace();
                    } catch (SmackException.NotConnectedException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        edtMessage.addTextChangedListener(textWatcher);

        edtMessage.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    try {
                        qbChatDialog.sendStopTypingNotification();
                    } catch (XMPPException e) {
                        e.printStackTrace();
                    } catch (SmackException.NotConnectedException e) {
                        e.printStackTrace();
                    }
                    return true;
                }
                return false;
            }
        });
    }
    private void addMessageStateListener(){
        messageStatusListener=new QBMessageStatusListener() {
            @Override
            public void processMessageDelivered(String s, String s1, Integer integer) {
                txt_status.setText("Online");
                adapter.changeMessageStatus(s,integer,0);
            }

            @Override
            public void processMessageRead(String s, String s1, Integer integer) {
                Log.d(Tag,s+" "+s1+integer);
                adapter.changeMessageStatus(s,integer,0);
                txt_status.setText("Online");
            }
        };
        messageStatusesManager.addMessageStatusListener(messageStatusListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            Constants.currentDialog="";
            qbChatDialog.removeIsTypingListener(typingListener);
            qbChatDialog.removeMessageListrener(messageListener);
            messageStatusesManager.removeMessageStatusListener(messageStatusListener);
            edtMessage.removeTextChangedListener(textWatcher);
            mUserOnline.removeCallbacks(getUserOnline);
            stopService(new Intent(getApplicationContext(),ChatConenctionService.class));
            unregisterReceiver(internetReceiver);
            //pingManager.removePingFailedListener(pingFailedListener);

        }
        catch (Exception e){}
    }

    @Override
    public void onBackPressed() {
        if(from.equals("fromservice")){
            Intent i = getBaseContext().getPackageManager()
                    .getLaunchIntentForPackage( getBaseContext().getPackageName() );
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(i);
//            Intent intent=new Intent(getApplicationContext(),LoginActivity.class);
//            //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            finish();
//            startActivity(intent);
        }
        else{
           super.onBackPressed();
            finish();
        }


    }
    public static String getRandomString(int length) {
        final String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_+";
        StringBuilder result = new StringBuilder();
        while(length > 0) {
            Random rand = new Random();
            result.append(characters.charAt(rand.nextInt(characters.length())));
            length--;
        }
        return result.toString();
    }
    private long getUnixTime(){
        Calendar cal = Calendar.getInstance();

        TimeZone timeZone =  cal.getTimeZone();


        Date cals =    Calendar.getInstance(TimeZone.getDefault()).getTime();

        long milliseconds =   cals.getTime();

        milliseconds = milliseconds + timeZone.getOffset(milliseconds);


        long unixTimeStamp = milliseconds / 1000L;
        return unixTimeStamp;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_attach:
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    showMenuBelowLollipop();
                else
                    showMenu();
                break;
            case R.id.menu_attachment_document:
                hideMenu();
                getDocuments();
                break;
            case R.id.menu_attachment_camera:
                hideMenu();
                getAudio();
                break;
            case R.id.menu_attachment_gallery:
                hideMenu();
                getImage();
                break;
//            ima
        }
    }
    void showMenuBelowLollipop() {
        int cx = (attachmentLayout.getLeft() + attachmentLayout.getRight());
        int cy = attachmentLayout.getTop();
        int radius = Math.max(attachmentLayout.getWidth(), attachmentLayout.getHeight());

        try {
            SupportAnimator animator = ViewAnimationUtils.createCircularReveal(attachmentLayout, cx, cy, 0, radius);
            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setDuration(300);

            if (isHidden) {
                //Log.e(getClass().getSimpleName(), "showMenuBelowLollipop");
                attachmentLayout.setVisibility(View.VISIBLE);
                animator.start();
                isHidden = false;
            } else {
                SupportAnimator animatorReverse = animator.reverse();
                animatorReverse.start();
                animatorReverse.addListener(new SupportAnimator.AnimatorListener() {
                    @Override
                    public void onAnimationStart() {
                    }

                    @Override
                    public void onAnimationEnd() {
                        //Log.e("MainActivity", "onAnimationEnd");
                        isHidden = true;
                        attachmentLayout.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onAnimationCancel() {
                    }

                    @Override
                    public void onAnimationRepeat() {
                    }
                });
            }
        } catch (Exception e) {
            //Log.e(getClass().getSimpleName(), "try catch");
            isHidden = true;
            attachmentLayout.setVisibility(View.INVISIBLE);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    void showMenu() {
       int cx = (attachmentLayout.getLeft() + attachmentLayout.getRight());
        //int cx = (attachmentLayout.getBottom()+attachmentLayout.getTop());
        int cy = attachmentLayout.getTop();
        int radius = Math.max(attachmentLayout.getWidth(), attachmentLayout.getHeight());

        if (isHidden) {
            Animator anim = android.view.ViewAnimationUtils.createCircularReveal(attachmentLayout, cx, cy, 0, radius);
            attachmentLayout.setVisibility(View.VISIBLE);
            anim.start();
            isHidden = false;
        } else {
            Animator anim = android.view.ViewAnimationUtils.createCircularReveal(attachmentLayout, cx, cy, radius, 0);
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    attachmentLayout.setVisibility(View.INVISIBLE);
                    isHidden = true;
                }
            });
            anim.start();
        }
    }

    private void hideMenu() {
        attachmentLayout.setVisibility(View.GONE);
        isHidden = true;
    }
    private boolean runtimepermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    ) {
                return false;
            }
        }
        return true;
    }
    private void getImage(){
        if(runtimepermission()){
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, ""), Pick_Image_From_Gallery_Request);
        }
        else{
            if(Build.VERSION.SDK_INT>=23){
                requestPermissions(new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE

                }, 10);
            }

        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 10) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                    ) {
                getImage();

            } else {
                //startImageCropper();
                Toast.makeText(getApplicationContext(), "Press Allow button to proceed", Toast.LENGTH_SHORT).show();
            }
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==Pick_Image_From_Gallery_Request&&
                resultCode==RESULT_OK&&data!=null&&data.getData()!=null){
            Uri uri=data.getData();
            uploadAttchment("photo",uri);

        }
        else if(requestCode==Pick_Documents_Request&&
                    resultCode==RESULT_OK&&data!=null&&data.getData()!=null){
                Uri uri=data.getData();

                uploadAttchment("document",uri);
        }
        else if(requestCode==RQS_RECORDING&&
                resultCode==RESULT_OK&&data!=null&&data.getData()!=null){
            Uri uri=data.getData();

            uploadAttchment("audio",uri);
        }
    }
    private void uploadAttchment(final String type, final Uri uri){
        counter=counter+1;
        try {
            filePath= PathUtil.getPath(getApplication(),uri);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        File file = null;
        Log.d("Path",filePath+"");
        if(type.equals("photo")){
            file=getCompressPhoto(uri);
        }
        else{
            file=new File(filePath);
        }
        File file1=file;
        final String name=file.getName();
        Boolean fileIsPublic = true;
        final NotificationCompat.Builder builder=showProgress(type);
        QBContent.uploadFileTask(file, fileIsPublic,"simple", new QBProgressCallback() {
            final int currentNotification=counter;
            @Override
            public void onProgressUpdate(int i) {
                    if(i!=100) {
                        builder.setProgress(100, i, false);
                        mNotifyManager.notify(currentNotification, builder.build());
                    }
                    else{
                        builder.setProgress(0, 0, false).setContentText("Message Sent");
                        mNotifyManager.notify(currentNotification, builder.build());
                    }


            }

        }).performAsync(new QBEntityCallback<QBFile>() {
            @Override
            public void onSuccess(QBFile qbFile, Bundle bundle) {
                // create a message
                QBChatMessage message = new QBChatMessage();
                message.setSaveToHistory(true); // Save a message to history
                QBAttachment attachment = new QBAttachment(type);
                attachment.setId(qbFile.getId().toString());
                attachment.setName(name);
                attachment.setContentType(filePath.substring(filePath.lastIndexOf(".")));
                message.addAttachment(attachment);
                message.setDialogId(qbChatDialog.getDialogId());
                Date currentDate = new Date();
                message.setProperty("name",pref.getName());
                message.setProperty("image",pref.getImage_Url());
                message.setBody(type);
                message.setDateSent(currentDate.getTime() / 1000);
                message.setSenderId(Integer.parseInt(pref.getQbid()));
                message.setRecipientId(opponent_user);
                message.setProperty("file_path",filePath);
                sendMessage(message);
                //
            }

            @Override
            public void onError(QBResponseException e) {
                //Log.d("errorFileUpload",e.getMessage());
            }
        });
    }
    private void getDocuments(){
//        Intent intent = new Intent();
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        intent.setType("application/pdf||application/msword");
//        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//        startActivityForResult(intent,Pick_Documents_Request);
//        String[] mimeTypes =
//                {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
//                        "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
//                        "application/vnd.ms-excel","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
//                        "text/plain",
//                        "application/pdf",
//                        "application/zip"};
        String[] mimeTypes =
                {"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                        "application/vnd.ms-powerpoint","application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                        "text/plain",
                        "application/pdf"};

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            intent.setType(mimeTypesStr.substring(0,mimeTypesStr.length() - 1));
        }
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(Intent.createChooser(intent,"ChooseFile"), Pick_Documents_Request);
        }
        else{
            Toast.makeText(this, "No App Found for Document", Toast.LENGTH_SHORT).show();
        }

    }
    private void getDialog(){
        if(!QBChatService.getInstance().isLoggedIn()) {
            //Toast.makeText(this, "here"+QBChatService.getInstance().isLoggedIn(), Toast.LENGTH_SHORT).show();
            loginToChat();
        }
        else{
            fecthDialog();
        }

    }
    private void loginToChat(){
        if(App.getInstance().getPref().getToken()!=null) {
            QBUser user = new QBUser();
            user.setLogin(App.getInstance().getPref().getEmail());
            user.setPassword(App.getInstance().getPref().getPass());
            user.setId(Integer.parseInt(App.getInstance().getPref().getQbid()));
            QBChatService.getInstance().login(user, new QBEntityCallback() {
                @Override
                public void onSuccess(Object o, Bundle bundle) {
                    //Toast.makeText(App.getContext(), "Login", Toast.LENGTH_SHORT).show();
                    fecthDialog();
                }

                @Override
                public void onError(QBResponseException errors) {
                   // Log.d("Majorc",errors.toString());
                    if (errors.getMessage().contains("Connection failed")) {
                        new LovelyInfoDialog(ChatActivity.this)
                                .setTopColorRes(R.color.colorPrimary)
                                .setIcon(R.drawable.ic_alert)
                                //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                                .setTitle("No Internnet")
                                .setMessage("Please Connect to Internet")
                                .show();
                    }


                }
            });
        }
    }
    private void signIn(){
        QBUser user=new QBUser(App.getInstance().getPref().getEmail().toString(),
                App.getInstance().getPref().getPass().toString());
        // Login
        QBUsers.signIn(user).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle args) {
               // Log.d("Qb",user.getId().toString());
                getDialog();


            }

            @Override
            public void onError(QBResponseException error) {

                //Log.d("Qb",error.getMessage());
                if (error.getMessage().contains("Connection failed")) {
                    new LovelyInfoDialog(ChatActivity.this)
                            .setTopColorRes(R.color.colorPrimary)
                            .setIcon(R.drawable.ic_alert)
                            //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                            .setTitle("No Internnet")
                            .setMessage("Please Connect to Internet")
                            .show();
                }
            }
        });
    }
    private boolean checkSignIn() {
        return QBSessionManager.getInstance().getSessionParameters() != null;
    }
    private void getAudio(){
        Intent intent = new Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, RQS_RECORDING);
        }
        else{
            Toast.makeText(this, "No App Found for Sound Recording", Toast.LENGTH_SHORT).show();
        }

    }
    private void checkUserOnline(){
        try {
            long lastUserActivity = QBChatService.getInstance().getLastUserActivity(opponent_user);

            //returns last activity in seconds or 0 if user online or error (e.g. user never loggedin chat)
            if(lastUserActivity>60){
                txt_status.setText("Offline");
            }
            else{
                txt_status.setText("Online");
            }
        } catch (XMPPException.XMPPErrorException | SmackException.NotConnectedException e) {
            //handle error
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
        }
    }
    private void fecthDialog(){
        QBRestChatService.getChatDialogById(id).performAsync(
                new QBEntityCallback<QBChatDialog>() {
                    @Override
                    public void onSuccess(QBChatDialog dialog, Bundle params) {
                        Log.d("Got","dialog");

                        qbChatDialog=dialog;
                        variableInitialization();
                    }

                    @Override
                    public void onError(QBResponseException responseException) {

                    }
                });
    }
    private void InternetReceiver(){
        internetReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("Here",""+count);
                if(count==0) {
                    count=count+1;
                }
                else{
                    if (ConnectivityCheck.getInstance().isConnected(App.getContext())) {
                        if (from.equals("fromservice")) {
                            if (checkSignIn()) {
                                getDialog();
                            } else {
                                signIn();
                            }
                        }
                    }
                }
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(internetReceiver, filter);
    }
    private void GenerateNotification(String message){
        String CHANNEL_ID = "my_channel_01";
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("New Message")
                        .setContentText(message);
// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, ChatActivity.class);
        resultIntent.putExtra("check","");
// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from    the Activity leads out of
// your app to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(ChatActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mBuilder.setContentText("New Message")
                .setContentText(message)
                .setFullScreenIntent(resultPendingIntent, true)
                .setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle())
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true);
        mNotificationManager.notify(1, mBuilder.build());
    }
    private NotificationCompat.Builder showProgress(String type){

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, String.valueOf(counter))
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(type)
                        .setContentText("Sending in Progress").setProgress(100,0,false);

        return mBuilder;

    } private File getCompressPhoto(Uri uri){
        InputStream imageStream = null;
        try {
            imageStream = getContentResolver().openInputStream(
                    uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        Bitmap bmp = BitmapFactory.decodeStream(imageStream);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        // bmp.compress(Bitmap.CompressFormat.JPEG, 20, stream);
        bmp.compress(Bitmap.CompressFormat.JPEG, 20, stream);
        byte[] byteArray = stream.toByteArray();
        File file=null;
        try {
             file=new File(Constants.fullpath+getFileName(uri));
            FileOutputStream fileOuputStream = new FileOutputStream(file);
            fileOuputStream.write(byteArray);
            fileOuputStream.flush();
            fileOuputStream.close();
            imageStream.close();
            stream.close();
        }  catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("Name",file.getName()+"");
        return file;
    }

    @Override
    protected void onPause() {
        super.onPause();
        Constants.isInBackground=true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.isInBackground=false;
    }
    private String getFileName(Uri uri){
        try {
            File file=new File(PathUtil.getPath(App.getContext(),uri));
            return file.getName().toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }
}
