package com.supertec.superofficeapp.activities.rehanActivities;

import android.app.ProgressDialog;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.ConnectivityCheck;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.ConstantClasses.IResult;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.ConstantClasses.Toasts;
import com.supertec.superofficeapp.ConstantClasses.VolleyService;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.activities.LoginActivity;
import com.supertec.superofficeapp.models.EmployeeModel;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class TaskAssign extends AppCompatActivity {
    private Button btn_assign_task;
    private CircleImageView user_image;
    private TextView txt_user_name;
    private MaterialEditText input_task_name,input_task_description;
    private EmployeeModel user;
    private IResult mIResult;
    private VolleyService mVolleyService;
    SharedPref sharedPref;
    private Toolbar toolbar;
    private ProgressDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_assign_activity);
        if(savedInstanceState!=null){
            onBackPressed();
        }
        callBack();
        varibaleInitialization();
    }
    private void varibaleInitialization(){
        mDialog=new ProgressDialog(this);
        mDialog.setTitle("Please Wait");
        mDialog.setCancelable(false);
        user=(EmployeeModel) getIntent().getSerializableExtra("user");
        sharedPref=new SharedPref(getApplicationContext());
        btn_assign_task=(Button) findViewById(R.id.btn_assign_task);
        user_image=(CircleImageView) findViewById(R.id.user_image);
        Picasso.with(App.getContext()).load(Constants.base_url+user.getmEmployeeImageUrl())
                .into(user_image);
        txt_user_name=(TextView) findViewById(R.id.txt_user_name);
        input_task_name=(MaterialEditText) findViewById(R.id.task_name);
        input_task_description=(MaterialEditText) findViewById(R.id.txt_task_description);
        toolbar = (Toolbar) findViewById(R.id.navigation_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setTitle("Task Assign");
        txt_user_name.setText(user.getmEmployeeName());
        mVolleyService=new VolleyService(mIResult,getApplicationContext());
        btn_assign_task.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ConnectivityCheck.getInstance().isConnected(App.getContext())) {
                    if (input_task_name.getText().length() > 0 && input_task_description.getText().length() > 0) {
                        assignTask();
                    } else {
                        new LovelyInfoDialog(TaskAssign.this)
                                .setTopColorRes(R.color.colorPrimary)
                                .setIcon(R.drawable.ic_alert)
                                //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                                .setTitle("Task assign Failed")
                                .setMessage("Task name or Task Description can not be empty")
                                .show();
                    }
                }
                else{
                    Toasts.getInstance().ShowNoConnectivityToast();
                }
            }
        });

    }
    public void assignTask(){
        mDialog.show();
        HashMap<String,String> map=new HashMap<>();
        map.put("user",user.getId());
        map.put("task_name",input_task_name.getText().toString());
        map.put("task_description",input_task_description.getText().toString());
        mVolleyService.postDataWithAuthorization("Assign Task","addnewtask",map,sharedPref.getToken());
    }
    public void callBack(){
        mIResult=new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                mDialog.dismiss();
                try {
                    JSONObject parentobj=new JSONObject(response);
                    JSONObject responseObj=parentobj.getJSONObject("response");
                    if(responseObj.getInt("status")==200){
                        input_task_name.setText("");
                        input_task_description.setText("");
                        Toast.makeText(getApplicationContext(), "Task assigned Succcessfully", Toast.LENGTH_SHORT).show();
                    }
                    else if(responseObj.getInt("status")==400){
                        Toast.makeText(getApplicationContext(), "User id is required", Toast.LENGTH_SHORT).show();
                    }
                    else if(responseObj.getInt("status")==401){
                        Toast.makeText(getApplicationContext(), "You are not authorized", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                mDialog.dismiss();
            }

            @Override
            public void notifyNoInternet(String requestType, String url, HashMap map, String token) {
                mDialog.dismiss();
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
