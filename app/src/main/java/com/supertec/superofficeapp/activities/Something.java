package com.supertec.superofficeapp.activities;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.messages.services.SubscribeService;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.activities.EmployeeActivities.EmployeeDashboard;
import com.supertec.superofficeapp.activities.rehanActivities.RehanDashboard;
import com.supertec.superofficeapp.services.EmployeeLocationMonitor;
import com.supertec.superofficeapp.services.PermanentService;

public class Something extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        signIn();
    }
    public void gotoNextActivity(){
        if(App.getInstance().getPref().getUser_Type().equals("rehan")){
            Intent intent=new Intent(getApplicationContext(), RehanDashboard.class);
            startActivity(intent);
        }
        else if(App.getInstance().getPref().getUser_Type().equals("employee")){
            startService(new Intent(getApplicationContext(), EmployeeLocationMonitor.class));
            if(Build.VERSION.SDK_INT>=24){
                startService(new Intent(getApplicationContext(), PermanentService.class));
            }
            Intent intent=new Intent(getApplicationContext(), EmployeeDashboard.class);
            startActivity(intent);
        }
    }
    private void signIn(){
        QBUser user=new QBUser(App.getInstance().getPref().getEmail().toString(),
                App.getInstance().getPref().getPass().toString());
        // Login
        QBUsers.signIn(user).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser user, Bundle args) {
                Log.d("Qb",user.getId().toString());
                gotoNextActivity();

            }

            @Override
            public void onError(QBResponseException error) {
                Log.d("Qb",error.getMessage());
            }
        });
    }
}
