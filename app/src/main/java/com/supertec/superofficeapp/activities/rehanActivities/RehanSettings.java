package com.supertec.superofficeapp.activities.rehanActivities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.SharedPreferences.SettingsPref;

public class RehanSettings extends AppCompatActivity {
    private Switch mLocationSharing;
    private SharedPref pref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rehan_settings_activity);
        variableInitialization();
    }
    private void variableInitialization(){
        pref=new SharedPref(getApplicationContext());
        mLocationSharing=(Switch) findViewById(R.id.sw_location);
        handleSwitch();
    }
    private void handleSwitch(){
        mLocationSharing.setChecked(SettingsPref.getInstance().getLocationSharing());
        mLocationSharing.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SettingsPref.getInstance().setLocationSharing(isChecked);
                processLocationSharing();
            }
        });
    }
    private void processLocationSharing(){
        if(SettingsPref.getInstance().getLocationSharing()){
            Toast.makeText(App.getContext(), "Location Sharing On", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(App.getContext(), "Location Sharing Off", Toast.LENGTH_SHORT).show();
        }
    }
}
