package com.supertec.superofficeapp.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.api.ApiRoutes;
import com.supertec.superofficeapp.api.ApiService;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatePasswordActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private MaterialEditText input_oldpass,input_newpass;
    private Button btn_update;
    ApiRoutes updatePass;
    private ProgressDialog mDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_password_activity);
        variableInitialization();
    }
    private void variableInitialization(){
        mDialog=new ProgressDialog(this);
        mDialog.setCancelable(false);
        mDialog.setTitle("Please Wait");
        toolbar=(Toolbar) findViewById(R.id.navigation_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Update Password");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        updatePass= ApiService.getInstance().RootService(ApiRoutes.class);
        input_oldpass=(MaterialEditText) findViewById(R.id.input_old_password);
        input_newpass=(MaterialEditText) findViewById(R.id.input_new_password);
        btn_update=(Button) findViewById(R.id.btn_updatepass);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(input_oldpass.getText().length()>=8){
                    if(input_newpass.getText().length()>=8){
                        UpdatePassword();
                    }
                    else{
                        new LovelyInfoDialog(UpdatePasswordActivity.this)
                                .setTopColorRes(R.color.colorPrimary)
                                .setIcon(R.drawable.ic_alert)
                                //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                                .setTitle("Update Password Failed")
                                .setMessage("Please enter minimum 8 characters in new password field")
                                .show();
                    }
                }
                else{
                    new LovelyInfoDialog(UpdatePasswordActivity.this)
                            .setTopColorRes(R.color.colorPrimary)
                            .setIcon(R.drawable.ic_alert)
                            //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                            .setTitle("Update Password Failed")
                            .setMessage("Please enter minimum 8 characters in old password field")
                            .show();
                }
            }
        });
    }
    private void UpdatePassword(){
        mDialog.show();
        HashMap map=new HashMap();
        map.put("old_password",input_oldpass.getText().toString());
        map.put("new_password",input_newpass.getText().toString());
        Call<ResponseBody> UpdatePassCall=updatePass.UpdatePassword(map);
        UpdatePassCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    mDialog.dismiss();
                if(response.body()!=null) {
                    try {

                        JSONObject parentObj = new JSONObject(response.body().string());
                        JSONObject responseObj = parentObj.getJSONObject("response");
                        if (responseObj.getInt("status") == 200) {
                            updateQbpassword();
                        } else if (responseObj.getInt("status") == 301) {
                            new LovelyInfoDialog(UpdatePasswordActivity.this)
                                    .setTopColorRes(R.color.colorPrimary)
                                    .setIcon(R.drawable.ic_alert)
                                    //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                                    .setTitle("Update Password Failed")
                                    .setMessage("Old password is wrong")
                                    .show();
                        } else {
                            Toast.makeText(App.getContext(), responseObj.getInt("status") + " " +
                                    responseObj.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mDialog.dismiss();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    private void updateQbpassword(){
        mDialog.show();
        QBUser user=new QBUser();
        user.setId(Integer.parseInt(App.getInstance().getPref().getQbid()));
        user.setOldPassword(input_oldpass.getText().toString());
        user.setPassword(input_newpass.getText().toString());
        QBUsers.updateUser(user).performAsync(new QBEntityCallback<QBUser>() {
            @Override
            public void onSuccess(QBUser qbUser, Bundle bundle) {
                App.getInstance().getPref().setPass(input_newpass.getText().toString());
                mDialog.dismiss();
                Log.d("Testing",App.getInstance().getPref().getPass());
                Toast.makeText(App.getContext(), "Password Changed Successfully", Toast.LENGTH_SHORT).show();
                input_oldpass.setText("");
                input_newpass.setText("");
            }

            @Override
            public void onError(QBResponseException e) {
                mDialog.dismiss();
            }
        });
    }
}
