package com.supertec.superofficeapp.activities.rehanActivities;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.ConnectivityCheck;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.ConstantClasses.Toasts;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.api.ApiRoutes;
import com.supertec.superofficeapp.api.ApiService;
import com.supertec.superofficeapp.models.EmployeeModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeCurrentLocationActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {
    private GoogleMap mMap;
    private Marker mMarker = null;
    SupportMapFragment mapFragment;
    private ImageView btn_back;
    private ApiRoutes mEmployeeLocation;
    private EmployeeModel user;
    private TextView txt_emp_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.employee_current_location_activity);
        if(savedInstanceState!=null){
            onBackPressed();
        }
        variableInitialization();
    }
    private void variableInitialization(){
        txt_emp_name=(TextView) findViewById(R.id.employee_name);
        user=(EmployeeModel) getIntent().getSerializableExtra("user");
        txt_emp_name.setText(user.getmEmployeeName().toString());
        mEmployeeLocation= ApiService.getInstance().RootService(ApiRoutes.class);
        btn_back=(ImageView) findViewById(R.id.btn_back);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        btn_back.setOnClickListener(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        getEmployeeLocation();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                onBackPressed();
                break;
        }
    }
    private void getEmployeeLocation(){
        HashMap map=new HashMap();
        map.put("user_id",user.getId());
        Call<ResponseBody> rehanLocationCall=mEmployeeLocation.getEmployeeLocation(map);
        rehanLocationCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.body()!=null){
                    try {
                        JSONObject parentobj=new JSONObject(response.body().string());
                        JSONObject responseObj=parentobj.getJSONObject("response");
                        if(responseObj.getInt("status")==200){
                            callBackProcessing(parentobj.getJSONObject("result"));
                        }
                        else{
                            Toast.makeText(App.getContext(),responseObj.getInt("status") +" "+
                                    responseObj.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
    private void callBackProcessing(JSONObject objResult){
        try {
            JSONObject location=objResult.getJSONObject("location").getJSONObject("current_location");
            double lat=location.getDouble("current_lat");
            double lng=location.getDouble("current_lng");
            if(lat==0&&lng==0){
                Toast.makeText(this, "This user has not signed in ", Toast.LENGTH_LONG).show();
            }
            else {
                addSingleMarker(new LatLng(lat, lng));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void addSingleMarker(LatLng latLng) {
        if (mMarker == null) {
            MarkerOptions options = new MarkerOptions().position(latLng).title(user.getmEmployeeName()).icon(
                    BitmapDescriptorFactory.fromResource(R.drawable.ic_action_rehan_location)
            );

            mMarker = mMap.addMarker(options);
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
