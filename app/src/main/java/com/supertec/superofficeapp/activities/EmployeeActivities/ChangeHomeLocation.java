package com.supertec.superofficeapp.activities.EmployeeActivities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.ConnectivityCheck;
import com.supertec.superofficeapp.ConstantClasses.EmployeeLocation;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.ConstantClasses.Toasts;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.api.ApiRoutes;
import com.supertec.superofficeapp.api.ApiService;
import com.supertec.superofficeapp.services.EmployeeLocationMonitor;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeHomeLocation extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {
    private GoogleMap mMap;
    private SharedPref pref;
    private Marker mMarker = null;
    SupportMapFragment mapFragment;
    private ImageView btn_back,btn_update;
    private ApiRoutes updateHomeLocation;
    private ProgressDialog mDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_home_location_activity);
        variableInitialization();

    }
    private void variableInitialization(){
        mDialog=new ProgressDialog(this);
        mDialog.setTitle("Please Wait");
        mDialog.setCancelable(false);
        pref=new SharedPref(getApplicationContext());
        updateHomeLocation= ApiService.getInstance().RootService(ApiRoutes.class);
        btn_back=(ImageView) findViewById(R.id.btn_back);
        btn_update=(ImageView) findViewById(R.id.btn_update);
         mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        btn_update.setOnClickListener(this);
        btn_back.setOnClickListener(this);


    }

    @Override
    protected void onDestroy() {
        btn_back.setImageDrawable(null);
        btn_update.setImageDrawable(null);
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        LatLng latLng=new LatLng(pref.getHome_lat(),pref.getHome_lng());
        mMarker = mMap.addMarker(new MarkerOptions().position(latLng).title("Your Home").icon(
                BitmapDescriptorFactory.fromResource(R.drawable.ic_action_rehan_location)
        ));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));
        mMap.setMyLocationEnabled(true);
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                    mMarker.remove();
                    mMarker = mMap.addMarker(new MarkerOptions().position(latLng).title("Your Home").icon(
                            BitmapDescriptorFactory.fromResource(R.drawable.ic_action_rehan_location)
                    ));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18.0f));
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_back:
                onBackPressed();
                break;
            case R.id.btn_update:
                if(ConnectivityCheck.getInstance().isConnected(App.getContext())){
                    updateLocation();
                }
                else{
                    Toasts.getInstance().ShowNoConnectivityToast();
                }

                break;
        }
    }
    private void updateLocation(){
        mDialog.show();
        HashMap map=new HashMap();
        map.put("lat",String.valueOf(mMarker.getPosition().latitude));
        map.put("lng",String.valueOf(mMarker.getPosition().longitude));
        Call<ResponseBody> updateLocationCall=updateHomeLocation.UpdateHomeLocation(map);
        updateLocationCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                mDialog.dismiss();
                if(response.body()!=null) {
                    try {
                        JSONObject parentObj = new JSONObject(response.body().string());
                        Log.d("Retro", parentObj.toString());
                        JSONObject responseObj = parentObj.getJSONObject("response");
                        if (responseObj.getInt("status") == 200) {
                            App.getInstance().getPref().setHome_lat(Float.parseFloat(String.valueOf(mMarker.getPosition().latitude)));
                            App.getInstance().getPref().setHome_lng(Float.parseFloat(String.valueOf(mMarker.getPosition().longitude)));
//                            stopService(new Intent(getApplicationContext(), EmployeeLocationMonitor.class));
//                            startService(new Intent(getApplicationContext(), EmployeeLocationMonitor.class));
                            if(Build.VERSION.SDK_INT>=26) {
                                EmployeeLocation.getInstance().removeGeoFences();
                                EmployeeLocation.getInstance().AddGeoFences();
                            }
                            Toast.makeText(App.getContext(), "Location Update Successfully", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(App.getContext(), parentObj.getInt("status") + " "
                                    + parentObj.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mDialog.dismiss();
            }
        });
    }
}
