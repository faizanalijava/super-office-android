package com.supertec.superofficeapp.activities;

import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.IResult;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.ConstantClasses.VolleyService;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.adapters.Task_update_Adapter;
import com.supertec.superofficeapp.api.ApiRoutes;
import com.supertec.superofficeapp.api.ApiService;
import com.supertec.superofficeapp.models.TaskModel;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Task_detail_new extends AppCompatActivity {
    private TextView mTextDescription;
    private Toolbar mToolbar;
    private RecyclerView mUpdates;
    private Task_update_Adapter adapter;
    private VolleyService mVolley;
    private IResult mIResult;
    private SharedPref pref;
    private TaskModel model;
    private Gson gson;
    LinearLayout add_update_layout;
    private ImageButton btn_send;
    private MaterialEditText input_update_description;
    private Menu menu;
    private ApiRoutes updateTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_detail_new_activity);
        if(savedInstanceState!=null){
            onBackPressed();
        }
        variableInitialization();
    }
    private void variableInitialization(){
        model=(TaskModel) getIntent().getSerializableExtra("task");
        updateTask= ApiService.getInstance().RootService(ApiRoutes.class);
        gson=new GsonBuilder().create();
        callBack();
        pref=new SharedPref(getApplicationContext());
        mVolley=new VolleyService(mIResult,getApplicationContext());
        mTextDescription=(TextView) findViewById(R.id.txt_task_description);
        mToolbar=(Toolbar) findViewById(R.id.navigation_toolbar);
        mUpdates=(RecyclerView) findViewById(R.id.task_updates_rc);
        add_update_layout=(LinearLayout) findViewById(R.id.textView4);
        input_update_description=(MaterialEditText) findViewById(R.id.input_update_description);
        btn_send=(ImageButton)findViewById(R.id.btn_send);
        if(pref.getUser_Type().equals("rehan")||model.getTask_status().equals("completed")){
            add_update_layout.setVisibility(View.INVISIBLE);
        }

        adapter=new Task_update_Adapter();
        mUpdates.setHasFixedSize(false);
        mUpdates.setNestedScrollingEnabled(false);
        mUpdates.setLayoutManager(new LinearLayoutManager(App.getInstance().getApplicationContext()));
        mUpdates.setAdapter(adapter);
        setSupportActionBar(mToolbar);
        mTextDescription.setText(model.getTask_Description());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(model.getTask_name());
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar();
//        getSupportActionBar()
//                .setBackgroundDrawable(new ColorDrawable
//                                (ContextCompat.getColor(getApplicationContext(), R.color.newtoolabar)));
//        mToolbar.setTitleTextColor();
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(input_update_description.getText().length()>0){
                    sendUpdates();
                }
                else{
                    new LovelyInfoDialog(Task_detail_new.this)
                            .setTopColorRes(R.color.colorPrimary)
                            .setIcon(R.drawable.ic_alert)
                            //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                            .setTitle("Update Failed")
                            .setMessage("Update Description can not be empty")
                            .show();
                }
            }
        });

    }
    private void callBack(){
        mIResult=new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                if(requestType.equals("get Updates")){
                    showUpdates(response);
                }
                else if(requestType.equals("add updates")){
                    callBackForNewUpdate(response);
                }
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {

            }

            @Override
            public void notifyNoInternet(String requestType, String url, HashMap map, String token) {

            }
        };

    }
    private void showUpdates(String response){
        try {
            JSONObject parentObj=new JSONObject(response);
            JSONObject responseObj=parentObj.getJSONObject("response");
            if(responseObj.getInt("status")==200){
                JSONObject resultObj=parentObj.getJSONObject("result");
                List<TaskModel.TaskUpdates> updates= Arrays.asList(gson.fromJson(
                        resultObj.getJSONArray("updates").toString(),TaskModel.TaskUpdates[].class
                ));
                //Toast.makeText(getContext(), ""+updates.size(), Toast.LENGTH_SHORT).show();
                if(updates.size()>0){
                    adapter.setUpdates(updates);
                }
                else{
                    Toast.makeText(App.getContext(), "no Updates Found for this task", Toast.LENGTH_SHORT).show();
                }


            }
            else{
                Toast.makeText(App.getInstance().getApplicationContext(),responseObj.getInt("status")+" "+responseObj
                        .getString("message"), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void callBackForNewUpdate(String response){
        try {
            JSONObject parentObj=new JSONObject(response);
            JSONObject responseObj=parentObj.getJSONObject("response");
            if(responseObj.getInt("status")==200){
                Toast.makeText(getApplicationContext(), "Operation Successfull", Toast.LENGTH_SHORT).show();
                input_update_description.setText("");
                getUpdates();
            }
            else{
                Toast.makeText(getApplicationContext(), responseObj.getInt("status")+"  "+
                        responseObj.getString("message"), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void getUpdates(){
        HashMap map=new HashMap();
        map.put("task_id",model.getTask_id());
        mVolley.postDataWithAuthorization("get Updates",
                "gettaskupdates",map,pref.getToken());
    }

    @Override
    protected void onStart() {
        super.onStart();
        getUpdates();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    private void sendUpdates(){
        HashMap map=new HashMap();
        map.put("task_id",model.getTask_id());
        map.put("update_description",input_update_description.getText().toString());
        mVolley.postDataWithAuthorization("add updates",
                "giveupdates",map,pref.getToken());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(model.getTask_status().equals("in progress")&&pref.getUser_Type().equals("employee")) {
            this.menu = menu;
            getMenuInflater().inflate(R.menu.task_done, menu);
            return true;
        }
        else if(model.getTask_status().equals("Approval request")&&pref.getUser_Type().equals("rehan")) {
            this.menu = menu;
            getMenuInflater().inflate(R.menu.rehan_task_menu, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.task_done_status:
                if(pref.getUser_Type().equals("rehan")) {
                    updateTaskStatus("completed");
                }
                else if(pref.getUser_Type().equals("employee")){
                    updateTaskStatus("Approval request");
                }
                break;
            case R.id.task_cancel_status:
                if(pref.getUser_Type().equals("rehan")) {
                    updateTaskStatus("in progress");
                }
                else if(pref.getUser_Type().equals("employee")){
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    private void updateTaskStatus(String status){
        HashMap map=new HashMap();
        map.put("task_id",model.getTask_id());
        map.put("status",status);
        Call<ResponseBody> updateStatusCallBack=updateTask.UpdateTaskStatus(map);
        updateStatusCallBack.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.body()!=null) {
                    try {
                        JSONObject object=new JSONObject(response.body().string());
                        JSONObject responseObj=object.getJSONObject("response");
                        if(responseObj.getInt("status")==200){
                            menu.setGroupVisible(R.id.task_menu_group, false);
                            add_update_layout.setVisibility(View.INVISIBLE);
                            Toast.makeText(App.getContext(), "" +responseObj.getString("message") , Toast.LENGTH_SHORT).show();
                        }
                        else{
                            Toast.makeText(App.getContext(), "" +responseObj.getString("message") , Toast.LENGTH_SHORT).show();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
