package com.supertec.superofficeapp.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.adapters.CreateNewDialogAdapter;
import com.supertec.superofficeapp.api.ApiRoutes;
import com.supertec.superofficeapp.api.ApiService;
import com.supertec.superofficeapp.models.EmployeeModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateNewDialog extends AppCompatActivity {
    private Toolbar mToolbar;
    private RecyclerView mUsersList;
    private CreateNewDialogAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_new_dialog_activity);
        variableInitialization();
    }
    private void variableInitialization(){
        adapter=new CreateNewDialogAdapter(this);
        mToolbar=(Toolbar) findViewById(R.id.navigation_toolbar);
        mUsersList=(RecyclerView) findViewById(R.id.users_list);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getSupportActionBar().setTitle("Select User");
        mUsersList.setHasFixedSize(true);
        mUsersList.setLayoutManager(new LinearLayoutManager(App.getContext()));
        mUsersList.setAdapter(adapter);
        getAllUsers();
    }
    private void getAllUsers(){
        ApiRoutes getAllUsers= ApiService.getInstance().RootService(ApiRoutes.class);
        Call<ResponseBody> callBack=getAllUsers.getallcolleague();
        callBack.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body()!= null) {
                    try {
                        JSONObject mParentObj = new JSONObject(response.body().string());
                        JSONObject mResponseObj = mParentObj.getJSONObject("response");
                        if (mResponseObj.getInt("status") == 200) {
                            Gson gson = new GsonBuilder().create();
                            JSONObject mResultObj = mParentObj.getJSONObject("result");
                            String users = mResultObj.getJSONArray("users").toString();
                            Log.d("users", users);
                            List<EmployeeModel> employees = new ArrayList<>(Arrays.asList(gson.fromJson(users
                                    , EmployeeModel[].class)));
//                        for (EmployeeModel model:
//                             employees) {
//                            Log.d("entries",""+model.getmEmployeeemail()+model.getmEmployeeImageUrl());
//                        }
                            if (employees.size() > 0) {
                                adapter.setList(employees);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
