package com.supertec.superofficeapp.activities.EmployeeActivities;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import okhttp3.Request;

import com.bumptech.glide.Glide;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quickblox.auth.QBAuth;
import com.quickblox.chat.QBChatService;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.messages.services.QBPushManager;
import com.quickblox.messages.services.SubscribeService;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.squareup.picasso.Picasso;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.ChatSetup;
import com.supertec.superofficeapp.ConstantClasses.ConnectivityCheck;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.ConstantClasses.EmployeeLocation;
import com.supertec.superofficeapp.ConstantClasses.PathUtil;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.ConstantClasses.UpdateConversation;
import com.supertec.superofficeapp.ConstantClasses.UploadImage;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.activities.ChatActivity;
import com.supertec.superofficeapp.activities.LoginActivity;
import com.supertec.superofficeapp.activities.rehanActivities.RehanDashboard;
import com.supertec.superofficeapp.api.ApiRoutes;
import com.supertec.superofficeapp.api.ApiService;
import com.supertec.superofficeapp.fragments.EmployeeFragments.EmployeeSettingsFragment;
import com.supertec.superofficeapp.fragments.EmployeeFragments.Employee_Dashboard_Fragment;
import com.supertec.superofficeapp.fragments.EmployeeFragments.Employee_Task_Fragment;
import com.supertec.superofficeapp.fragments.EmployeeFragments.RehanLocationFragment;
import com.supertec.superofficeapp.fragments.MessagesFragment;
import com.supertec.superofficeapp.services.AllMessagesListener;
import com.supertec.superofficeapp.services.ChatConenctionService;
import com.supertec.superofficeapp.services.EmployeeLocationMonitor;
import com.supertec.superofficeapp.services.LiveLocationService;
import com.supertec.superofficeapp.services.PermanentService;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import org.jivesoftware.smack.SmackException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;

import okhttp3.Response;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

public class EmployeeDashboard extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle toggle;
    private Toolbar toolbar;
    private NavigationView view;
    private Class fragmentClass;
    private Fragment fragment = null;
    private SharedPref sharedPref;
    private View headerLayout;
    private ImageView headerImage;
    private TextView headerText;
    FragmentManager fragmentManager ;
    private BroadcastReceiver internetReceiver;
    private boolean isRegistered=false;
    String TAG=EmployeeDashboard.class.getSimpleName();
    private int count=0;
    ApiRoutes apiRoutes;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.employee_dashboard_activity);
        if(savedInstanceState!=null){
            ChatSetup.getInstance().InitializeChat();
        }
        initPushManager();
        variableInitialization();
        fragmentManager= getSupportFragmentManager();
        fragmentClass=Employee_Dashboard_Fragment.class;
        setFragment(view.getMenu().getItem(0));
        InternetReceiver();
        if(!QBChatService.getInstance().isLoggedIn()) {
            loginToChat();
        }
        else{
            startService(new Intent(getApplicationContext(),AllMessagesListener.class));
        }
        if(ConnectivityCheck.getInstance().isConnected(App.getContext())){
            verifyUser();
        }

    }
    public void variableInitialization(){
        apiRoutes= ApiService.getInstance().RootService(ApiRoutes.class);
        sharedPref=new SharedPref(getApplicationContext());
        //Toast.makeText(this, ""+sharedPref.getToken(), Toast.LENGTH_SHORT).show();
        toolbar=(Toolbar) findViewById(R.id.navigation_toolbar);
        setSupportActionBar(toolbar);
        drawerLayout=(DrawerLayout) findViewById(R.id.employee_activity);
        toggle=new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(toggle);
        view=(NavigationView) findViewById(R.id.nv_employee);
        headerLayout=view.getHeaderView(0);
        headerImage=(ImageView) headerLayout.findViewById(R.id.profile_image);
        headerText=(TextView) headerLayout.findViewById(R.id.profile_name);
        headerText.setText(sharedPref.getName());
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        view.setNavigationItemSelectedListener(this);
        Glide.with(getApplicationContext()).load(Constants.getInstance().base_url+sharedPref.getImage_Url())
                .into(
                headerImage
        );
        headerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startImageCropper();


            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        uncheckAllMenuItems();
        switch (item.getItemId()) {
            case R.id.employee_dashboard:
                toolbar.setTitle("Home");
                fragmentClass = Employee_Dashboard_Fragment.class;
                setFragment(item);
                break;
            case R.id.employee_task:
                fragmentClass = Employee_Task_Fragment.class;
                setFragment(item);
                break;
            case R.id.employee_rehan_location:
                fragmentClass = RehanLocationFragment.class;
                setFragment(item);
                break;
            case R.id.employee_settings:
                fragmentClass = EmployeeSettingsFragment.class;
                setFragment(item);
                break;
            case R.id.messages:
                toolbar.setTitle("Messages");
                fragmentClass = MessagesFragment.class;
                setFragment(item);
                break;
            case R.id.employee_logout:
                if(ConnectivityCheck.getInstance().isConnected(App.getContext())){
                    SignoutProcess();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Please Connect to Internet and try again", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                //fragmentClass = User_Dashboard_Fragment.class;
        }


        drawerLayout.closeDrawer(GravityCompat.START);
        drawerLayout.closeDrawers();

        return true;
    }
    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {

            drawerLayout.closeDrawer(GravityCompat.START);

        } else {
            moveTaskToBack(true);

        }

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(toggle.onOptionsItemSelected(item)){

            return true;
        }


        return super.onOptionsItemSelected(item);
    }
    private void uncheckAllMenuItems() {
        final Menu menu = view.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            if (item.hasSubMenu()) {
                SubMenu subMenu = item.getSubMenu();
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    subMenuItem.setChecked(false);
                }
            } else {
                item.setChecked(false);
            }
        }
    }
    public void setFragment(MenuItem item){
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }


        fragmentManager.beginTransaction().replace(R.id.frame, fragment).commit();
        item.setChecked(true);
        setTitle(item.getTitle());
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                uploadFile(resultUri);
                headerImage.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
    private void uploadFile(Uri uri) {
        // create upload service client
//        FileUploadService service =
//                ServiceGenerator.createService(FileUploadService.class);

        // https://github.com/iPaulPro/aFileChooser/blob/master/aFileChooser/src/com/ipaulpro/afilechooser/utils/FileUtils.java
        // use the FileUtils to get the actual file by uri
        String filePath="";
        try {
            filePath= PathUtil.getPath(getApplication(),uri);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Log.d("Path",filePath);
        final File file = new File(filePath);

        // create RequestBody instance from file
        RequestBody filePart =
                RequestBody.create(
                        MediaType.parse("image/*"),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("pic", file.getName(), filePart);

        // add another part within the multipart request
        String descriptionString = "hello, this is description speaking";
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        //final JSONObject obj=new JSONObject();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
                                      @Override
                                      public Response intercept(Interceptor.Chain chain) throws IOException {
                                          Request original = chain.request();

                                          Request request = original.newBuilder()
                                                  .header("Authorization", "JWT"+" "+sharedPref.getToken())
                                                  .method(original.method(), original.body())
                                                  .build();

                                          return chain.proceed(request);
                                      }
                                  });
        OkHttpClient mainClient = httpClient.build();

        RequestBody description =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM,"JWT "+ sharedPref.getToken());
        Retrofit.Builder builder=new Retrofit.Builder().baseUrl(Constants.getInstance().base_url)
                .client(mainClient);


        Retrofit retrofit=builder.build();
        UploadImage client=retrofit.create(UploadImage.class);
        // finally, execute the request

        Call<ResponseBody> call = client.uploadPhoto(description,body);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                if(response.body()!=null) {
                    try {
                        Log.d("Response", response.body().string());
                        sharedPref.setImage_Url(file.getName().toString());
                        Picasso.with(getApplicationContext()).load(Constants.getInstance().base_url + sharedPref.getImage_Url())
                                .into(
                                        headerImage
                                );
                        UpdateConversation.getInstance().update();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
    private boolean runtimepermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    ) {
                Log.d("iamin", "if");
                return false;
            }
        }
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 10) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                    ) {
                startImageCropper();

            } else {
                //startImageCropper();
                Toast.makeText(getApplicationContext(), "Press Allow button to proceed", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void startImageCropper(){
        if(runtimepermission()){
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(EmployeeDashboard.this);
        }
        else{
            if(Build.VERSION.SDK_INT>=23){
                requestPermissions(new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE

                }, 10);
            }

        }
    }
    private void loginToChat(){
        if(sharedPref.getToken()!=null) {
            QBUser user = new QBUser();
            user.setLogin(sharedPref.getEmail());
            user.setPassword(sharedPref.getPass());
            user.setId(Integer.parseInt(sharedPref.getQbid()));
            QBChatService.getInstance().login(user, new QBEntityCallback() {
                @Override
                public void onSuccess(Object o, Bundle bundle) {
                    Log.w("Major","Success");
                    startService(new Intent(getApplicationContext(), AllMessagesListener.class));
                   // Toast.makeText(App.getContext(), "Login", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(QBResponseException errors) {
                    //login failed
                    Log.w("Major1",errors.getMessage()+errors.getStackTrace()+errors.getCause());


                    if (errors.getMessage().contains("Connection failed")) {
                        new LovelyInfoDialog(EmployeeDashboard.this)
                                .setTopColorRes(R.color.colorPrimary)
                                .setIcon(R.drawable.ic_alert)
                                //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                                .setTitle("No Internnet")
                                .setMessage("Please Connect to Internet")
                                .show();
                    }
//                    else{
//                        loginToChat();
//                    }
                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(internetReceiver);
        stopService(new Intent(getApplicationContext(),AllMessagesListener.class));
        super.onDestroy();

    }
    private void InternetReceiver(){

        internetReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(count==0){
                    count=count+1;
                }
                else {
                    if (ConnectivityCheck.getInstance().isConnected(App.getContext())) {
                        if (!QBChatService.getInstance().isLoggedIn()) {
                            loginToChat();
                        }
                    }
                }
            }
        };
        IntentFilter filter = new IntentFilter();
                        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
                        registerReceiver(internetReceiver, filter);
    }

    private void SignoutProcess(){
        sendStatus();
//        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(App.getContext()));
//        dispatcher.cancel("emp-location");
        Constants.isInitialize=false;
        stopService(new Intent(getApplicationContext(),AllMessagesListener.class));
        stopService(new Intent(getApplicationContext(),LiveLocationService.class));
        stopService(new Intent(getApplicationContext(),PermanentService.class));
        EmployeeLocation.getInstance().removeGeoFences();
        if(QBPushManager.getInstance().isSubscribedToPushes()) {
            SubscribeService.unSubscribeFromPushes(getApplicationContext());
        }
        else{
            logoutFromApp();
        }

//        if(Build.VERSION.SDK_INT>=24){
//            stopService(new Intent(getApplicationContext(), PermanentService.class));
//        }
//        stopService(new Intent(getApplicationContext(), EmployeeLocationMonitor.class));

    }
    private void initPushManager() {
        QBPushManager.getInstance().addListener(new QBPushManager.QBSubscribeListener() {
            @Override
            public void onSubscriptionCreated() {
                Log.d(TAG, "SubscriptionCreated");

            }

            @Override
            public void onSubscriptionError(Exception e, int resultCode) {
                Log.d(TAG, "SubscriptionError" + e.getLocalizedMessage()+e.getStackTrace());
                if (resultCode >= 0) {
                    String error = GoogleApiAvailability.getInstance().getErrorString(resultCode);
                    Log.d(TAG, "SubscriptionError playServicesAbility: " + error);
                }
            }

            @Override
            public void onSubscriptionDeleted(boolean success) {
                Log.d("Deleted",String.valueOf(success));
                logoutFromApp();
            }
        });
    }
    private void logoutFromApp(){
        if(QBChatService.getInstance().isLoggedIn()) {
            QBChatService.getInstance().logout(new QBEntityCallback<Void>() {
                @Override
                public void onSuccess(Void aVoid, Bundle bundle) {
                    QBUsers.signOut().performAsync(new QBEntityCallback<Void>() {
                        @Override
                        public void onSuccess(Void result, Bundle args) {
                            sharedPref.clearEverything();
                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(intent);
                            finish();

                        }

                        @Override
                        public void onError(QBResponseException errors) {

                        }
                    });
                }

                @Override
                public void onError(QBResponseException e) {

                }
            });
        }
        else{
            sharedPref.clearEverything();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
    private void verifyUser(){
        HashMap map=new HashMap();
        map.put("password",sharedPref.getPass());
        Call<ResponseBody> verifyUserCall=apiRoutes.verifyUser(map);
        verifyUserCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                if(response.body()!=null){
                    try {
                        JSONObject parentobj=new JSONObject(response.body().string());
                        JSONObject responseObj=parentobj.getJSONObject("response");
                        Log.d("Imp",responseObj.toString());
                        if(responseObj.getInt("status")==401){
                            SignoutProcess();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
    private void sendStatus(){
        Call<ResponseBody> call=apiRoutes.upDateStaus("outside");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
//                int status=response.code();
//                if(status==200){
//                    try {
//                        Toast.makeText(EmployeeDashboard.this, ""+response.body().string(), Toast.LENGTH_SHORT).show();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//                else{
//                    try {
//                        Toast.makeText(EmployeeDashboard.this, ""+response.errorBody().string(), Toast.LENGTH_SHORT).show();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
