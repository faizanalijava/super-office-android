package com.supertec.superofficeapp.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.quickblox.auth.session.QBSessionManager;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.messages.services.SubscribeService;
import com.quickblox.users.QBUsers;
import com.quickblox.users.model.QBUser;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.ChatSetup;
import com.supertec.superofficeapp.ConstantClasses.ConnectivityCheck;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.ConstantClasses.EmployeeLocation;
import com.supertec.superofficeapp.ConstantClasses.JWTUtils;
import com.supertec.superofficeapp.ConstantClasses.RunTimePermissions;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.ConstantClasses.IResult;
import com.supertec.superofficeapp.ConstantClasses.Toasts;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.ConstantClasses.VolleyService;
import com.supertec.superofficeapp.SharedPreferences.SettingsPref;
import com.supertec.superofficeapp.activities.EmployeeActivities.EmployeeDashboard;
import com.supertec.superofficeapp.activities.rehanActivities.RehanDashboard;
import com.supertec.superofficeapp.services.EmployeeLocationMonitor;
import com.supertec.superofficeapp.services.EmployeeLocationUpdater;
import com.supertec.superofficeapp.services.LiveLocationService;
import com.supertec.superofficeapp.services.PermanentService;
import com.supertec.superofficeapp.services.RehanLocationService;
import com.wang.avi.AVLoadingIndicatorView;
import com.yarolegovich.lovelydialog.LovelyInfoDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;


public class LoginActivity extends AppCompatActivity {
    MaterialEditText input_email,input_password;
    Button btn_login;
    AVLoadingIndicatorView loadingIndicatorView;
    ConstraintLayout inside;
    IResult mIResult;
    VolleyService mVolleyService;
    SharedPref sharedPref;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!Constants.isInitialize) {
            ChatSetup.getInstance().InitializeChat();
        }
        sharedPref=new SharedPref(getApplicationContext());
        checkLogin();
        setContentView(R.layout.login_activity);
        callBack();
        checkPlayServices();
        variableInitialization();
    }
//    private void just(){
//        Toast.makeText(this, ""+Constants.isInitialize, Toast.LENGTH_SHORT).show();
//        Constants.isInitialize=true;
//        Toast.makeText(this, ""+Constants.isInitialize, Toast.LENGTH_SHORT).show();
//    }
    private void variableInitialization(){
        mVolleyService=new VolleyService(mIResult,getApplicationContext());
        input_email=(MaterialEditText) findViewById(R.id.input_email);
        input_password=(MaterialEditText) findViewById(R.id.input_password);
        btn_login=(Button) findViewById(R.id.btn_login);
        loadingIndicatorView=(AVLoadingIndicatorView) findViewById(R.id.loading);
        inside=(ConstraintLayout) findViewById(R.id.inside);
        inside.setVisibility(View.INVISIBLE);
//        input_email.setText("rehanlocal");
//        input_password.setText("12345678");
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ConnectivityCheck.getInstance().isConnected(App.getContext())) {
                    if (runtimepermission()) {
                        sendLoginRequest();
                    } else {
                        if (Build.VERSION.SDK_INT >= 23) {
                            requestPermissions(new String[]{
                                    Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.READ_EXTERNAL_STORAGE

                            }, 10);
                        }
                    }
                }
                else{
                    Toasts.getInstance().ShowNoConnectivityToast();
                }

            }
        });
    }
    private void sendLoginRequest(){
        if (input_email.getText().toString().length() > 0 && input_password.getText()
                .toString().length() > 0) {
            inside.setVisibility(View.VISIBLE);
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("email", input_email.getText().toString());
            map.put("password", input_password.getText().toString());
            mVolleyService.postDataVolley("Login", "signin", map);
        } else {
            new LovelyInfoDialog(LoginActivity.this)
                    .setTopColorRes(R.color.colorPrimary)
                    .setIcon(R.drawable.ic_alert)
                    //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                    .setTitle("Login Failed")
                    .setMessage("Username or Password can not be empty")
                    .show();
        }
    }
    private void callBack(){
        mIResult=new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                Log.d("Response",response);
                try {
                    JSONObject object=new JSONObject(response);
                    JSONObject objResponse=object.getJSONObject("response");
                    int status=objResponse.getInt("status");
                    if(status==200){
                        JSONObject objResult=object.getJSONObject("result");
                        sharedPref.setToken(objResult.getString("token"));
                        try {
                            String decoded=JWTUtils.decoded(objResult.getString("token"));
                            if(decoded!=null){
                                JSONObject objDecoded=new JSONObject(decoded);
                                sharedPref.setID(objDecoded.getString("_id"));
                                sharedPref.setName(objDecoded.getString("name"));
                                sharedPref.setEmail(objDecoded.getString("email"));
                                sharedPref.setUser_Type(objDecoded.getString("user_type"));
                                sharedPref.setImage_Url(objDecoded.getString("image_url"));

                                JSONObject parentLocationObj=objResult.getJSONObject("location");

                                JSONObject office_location=parentLocationObj.getJSONObject("office_location");
                                sharedPref.setOffice_lat(Float.parseFloat(office_location.getString("office_lat")));
                                sharedPref.setOffice_lng(Float.parseFloat(office_location.getString("office_lng")));

                                JSONObject home_location=parentLocationObj.getJSONObject("home_location");
                                sharedPref.setHome_lat(Float.parseFloat(home_location.getString("home_lat")));
                                sharedPref.setHome_lng(Float.parseFloat(home_location.getString("home_lng")));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //Log.d("values",""+sharedPref.getOffice_lat()+" "+sharedPref.getOffice_lng());
                        QBUser user=new QBUser(sharedPref.getEmail(),input_password.getText().toString());
                        // Login
                        QBUsers.signIn(user).performAsync(new QBEntityCallback<QBUser>() {
                            @Override
                            public void onSuccess(QBUser user, Bundle args) {
                                inside.setVisibility(View.INVISIBLE);
                                Log.d("Qb",user.getId().toString());
                                sharedPref.setQbid(user.getId().toString());
                                sharedPref.setPass(input_password.getText().toString());
                                SubscribeService.subscribeToPushes(App.getContext(), false);
                                if(sharedPref.getUser_Type().equals("employee")){
                                    //registerJobScheduler();

                                        if(RunTimePermissions.runtimepermission()) {

                                            if(Build.VERSION.SDK_INT>=26) {
                                                EmployeeLocation.getInstance().AddGeoFences();
                                            }
                                            else{
                                                App.getContext().startService(new Intent(App.getContext(), LiveLocationService.class));
                                            }
                                        }
                                    startService(new Intent(getApplicationContext(),PermanentService.class));
                                }
                                else if(sharedPref.getUser_Type().equals("rehan")){
                                    App.getContext().startService(new Intent(App.getContext(), LiveLocationService.class));
                                }
                                gotoNextActivity();

                            }

                            @Override
                            public void onError(QBResponseException error) {
                                inside.setVisibility(View.INVISIBLE);
                                Toast.makeText(LoginActivity.this, "Something went wrong please try again", Toast.LENGTH_SHORT).show();
                                Log.d("Qb",error.getMessage());
                            }
                        });
                        //Toast.makeText(LoginActivity.this, ""+sharedPref.getToken()+sharedPref.getID(), Toast.LENGTH_SHORT).show();
                    }
                    else{
                        checkError(status);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void notifyError(String requestType, VolleyError error) {
                inside.setVisibility(View.INVISIBLE);
                if(error.toString().contains("NoConnectionError")) {
                    Toast.makeText(getApplicationContext(), "Please Connect to internet", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void notifyNoInternet(String requestType, String url, HashMap map, String token) {

            }
        };
    }
    public void checkError(int status){
        inside.setVisibility(View.INVISIBLE);
        if(status==301){
            new LovelyInfoDialog(this)
                    .setTopColorRes(R.color.colorPrimary)
                    .setIcon(R.drawable.ic_alert)
                    //This will add Don't show again checkbox to the dialog. You can pass any ID as argument
                    .setTitle("Login Failed")
                    .setMessage("Username or Password is wrong")
                    .show();
        }
    }
    public void checkLogin(){
        if(sharedPref.getToken()!=null&&sharedPref.getQbid()!=null){
            if(checkSignIn()) {
                gotoNextActivity();
            }
            else{
                Intent intent=new Intent(getApplicationContext(),Something.class);
                startActivity(intent);
                finish();
            }
        }
    }
    public void gotoNextActivity(){
        if(sharedPref.getUser_Type().equals("rehan")){
            Intent intent=new Intent(getApplicationContext(), RehanDashboard.class);
            startActivity(intent);
            finish();
        }
        else if(sharedPref.getUser_Type().equals("employee")){
            startService(new Intent(getApplicationContext(),PermanentService.class));
            startService(new Intent(getApplicationContext(),LiveLocationService.class));
//            startService(new Intent(getApplicationContext(), EmployeeLocationMonitor.class));
//            if(Build.VERSION.SDK_INT>=24){
//                startService(new Intent(getApplicationContext(), PermanentService.class));
//            }
            Intent intent=new Intent(getApplicationContext(), EmployeeDashboard.class);
            startActivity(intent);
            finish();
        }
    }
    private boolean runtimepermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    ||ContextCompat.checkSelfPermission(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    ) {
                return false;
            }
        }
        return true;
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 10) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED&&grantResults[1]==
                    PackageManager.PERMISSION_GRANTED
                    ) {
                CreateDir();
                sendLoginRequest();

            } else {
                //startImageCropper();
                Toast.makeText(getApplicationContext(), "Press Allow button to proceed", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private boolean checkSignIn() {
        return QBSessionManager.getInstance().getSessionParameters() != null;
    }
    private void checkPlayServices(){
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
       // Toast.makeText(this, "h"+resultCode, Toast.LENGTH_SHORT).show();
        Log.d("Code",""+resultCode);
        if (resultCode != ConnectionResult.SUCCESS) {
            GoogleApiAvailability.getInstance().showErrorNotification(this,resultCode);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Toast.makeText(this, "Destroy", Toast.LENGTH_SHORT).show();
    }
    private void CreateDir(){
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), Constants.dir);
        //File mediaStorageDir = new File(getFilesDir(), Constants.dir);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("App","Exist");
            }
        }
        else{
            Log.d("App","not Exist");
        }
    }
    private void registerJobScheduler(){
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(App.getContext()));
        Job myJob = dispatcher.newJobBuilder()
                    // the JobService that will be called
                    .setService(EmployeeLocationUpdater.class)
                    // uniquely identifies the job
                    .setTag("emp-location")
                    // one-off job
                    .setRecurring(true)
                    // don't persist past a device reboot
                    .setLifetime(Lifetime.FOREVER)
                    // start between 0 and 60 seconds from now
                    .setTrigger(Trigger.executionWindow(0, 60))
                    // don't overwrite an existing job with the same tag
                    .setReplaceCurrent(false)
                    // retry with exponential backoff
                    .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                    // constraints that need to be satisfied for the job to run
                    .setConstraints(
                            // only run on an unmetered network
                            Constraint.ON_UNMETERED_NETWORK
                            // only run when the device is charging
                    )
                    .build();

            dispatcher.mustSchedule(myJob);
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
