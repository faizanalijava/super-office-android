package com.supertec.superofficeapp.PushNotification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.RemoteMessage;
import com.quickblox.chat.QBChatService;
import com.quickblox.chat.QBRestChatService;
import com.quickblox.chat.model.QBChatDialog;
import com.quickblox.core.QBEntityCallback;
import com.quickblox.core.exception.QBResponseException;
import com.quickblox.messages.services.fcm.QBFcmPushListenerService;
import com.quickblox.users.model.QBUser;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.activities.ChatActivity;
import com.supertec.superofficeapp.activities.LoginActivity;
import com.supertec.superofficeapp.services.ChatConenctionService;

import java.util.Map;

/**
 * Created by Programmer on 2/11/2018.
 */

public class FcmPushListenerService extends QBFcmPushListenerService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Map<String, String> pushData = remoteMessage.getData();
        String message = pushData.get("message");
        String dialog_id = pushData.get("dialog_id");
        Log.d("Message1",message);
        GenerateNotification(message,dialog_id);

    }
//    private void GenerateNotification(String message,String id){
//        String CHANNEL_ID = "my_channel_01";
//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(this, CHANNEL_ID)
//                        .setSmallIcon(R.mipmap.ic_launcher)
//                        .setContentTitle("New Message")
//                        .setContentText(message);
//// Creates an explicit intent for an Activity in your app
//        Intent resultIntent = new Intent(this, ChatActivity.class);
//        resultIntent.putExtra("check","fromservice");
//        resultIntent.putExtra("id",id);
//// The stack builder object will contain an artificial back stack for the
//// started Activity.
//// This ensures that navigating backward from the Activity leads out of
//// your app to the Home screen.
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//// Adds the back stack for the Intent (but not the Intent itself)
//        stackBuilder.addParentStack(ChatActivity.class);
//// Adds the Intent that starts the Activity to the top of the stack
//        stackBuilder.addNextIntent(resultIntent);
//        PendingIntent resultPendingIntent =
//                stackBuilder.getPendingIntent(
//                        0,
//                        PendingIntent.FLAG_UPDATE_CURRENT
//                );
//        mBuilder.setContentIntent(resultPendingIntent);
//        NotificationManager mNotificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//// mNotificationId is a unique integer your app uses to identify the
//// notification. For example, to cancel the notification, you can pass its ID
//// number to NotificationManager.cancel().
//        mNotificationManager.notify(1, mBuilder.build());
//    }
    private void GenerateNotification(String message,String id){
                String CHANNEL_ID = "my_channel_01";
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("New Message")
                        .setContentText(message);
// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, ChatActivity.class);
        resultIntent.putExtra("check","fromservice");
        resultIntent.putExtra("id",id);
// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from    the Activity leads out of
// your app to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(LoginActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_ONE_SHOT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mBuilder.setContentText("New Message")
                .setContentText(message)
                //.setFullScreenIntent(resultPendingIntent, true)
                .setStyle(new android.support.v4.media.app.NotificationCompat.MediaStyle())
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                //.setDefaults(Notification.DEFAULT_SOUND)
                .setVibrate(new long[] {0, 1000, 200,1000 })
                .setLights(Color.MAGENTA, 500, 500)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setAutoCancel(true);
        mNotificationManager.notify(1, mBuilder.build());
    }
}
