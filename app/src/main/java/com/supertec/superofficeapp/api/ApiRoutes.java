package com.supertec.superofficeapp.api;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Programmer on 1/27/2018.
 */

public interface ApiRoutes {
    @FormUrlEncoded
    @POST("rehan/updatelocation")
    Call<ResponseBody> UpdateRehanLocation(@FieldMap Map<String,String> params);
    @FormUrlEncoded
    @POST("employee/updatehomelocation")
    Call<ResponseBody> UpdateHomeLocation(@FieldMap Map<String,String> params);
    @FormUrlEncoded
    @POST("employee/updatecurrentlocation")
    Call<ResponseBody> UpdateCurrentLocation(@FieldMap Map<String,String> params);
    @FormUrlEncoded
    @POST("changepassword")
    Call<ResponseBody> UpdatePassword(@FieldMap Map<String,String> params);
    @FormUrlEncoded
    @POST("verifyuser")
    Call<ResponseBody> verifyUser(@FieldMap Map<String,String> params);
    @FormUrlEncoded
    @POST("employee/updatetaskstatus")
    Call<ResponseBody> UpdateTaskStatus(@FieldMap Map<String,String> params);
    @GET("employee/getrehanlocation")
    Call<ResponseBody> getRehanLocation();
    @FormUrlEncoded
    @POST("rehan/getemployeeLocation")
    Call<ResponseBody> getEmployeeLocation(@FieldMap Map<String,String> params);
    @GET("getallcolleague")
    Call<ResponseBody> getallcolleague();
    @GET("getrehan")
    Call<ResponseBody> getRehan();
    @FormUrlEncoded
    @POST("changestatus")
    Call<ResponseBody> upDateStaus(@Field("status") String status);
}
