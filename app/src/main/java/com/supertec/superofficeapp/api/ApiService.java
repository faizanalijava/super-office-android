package com.supertec.superofficeapp.api;

import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.Constants;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;

/**
 * Created by Programmer on 1/27/2018.
 */

public class ApiService {
    private static final ApiService ourInstance = new ApiService();
    private static SharedPref pref;
    public static ApiService getInstance() {
        return ourInstance;
    }

    private ApiService() {
        pref= new SharedPref(App.getContext());
    }
    public <S> S RootService(Class<S> serviceClass) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Authorization", "JWT"+" "+pref.getToken())
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });


        OkHttpClient mainClient = httpClient.build();
        Retrofit builder = new Retrofit.Builder()
                .baseUrl(Constants.getInstance().base_url)
                .client(mainClient)
                .build();

        return builder.create(serviceClass);
    }
}
