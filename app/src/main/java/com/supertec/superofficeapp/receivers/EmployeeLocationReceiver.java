package com.supertec.superofficeapp.receivers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.ConnectivityCheck;
import com.supertec.superofficeapp.ConstantClasses.IResult;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.ConstantClasses.VolleyService;
import com.supertec.superofficeapp.R;
import com.supertec.superofficeapp.SharedPreferences.NoInternetPref;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Programmer on 1/20/2018.
 */

public class EmployeeLocationReceiver extends BroadcastReceiver {
    String Tag="GeoFence";
    private static VolleyService mVolley;
    private static IResult mIResult;
    private static SharedPref pref;
    private NotificationManager mNotifyManager;
    @Override
    public void onReceive(Context context, Intent intent) {
        mNotifyManager =
                (NotificationManager) App.getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        Log.d("Trigger","");
        //Toast.makeText(context, "tRIGGER    ", Toast.LENGTH_LONG).show();
        callBack();
        mVolley=new VolleyService(mIResult,context);
        pref=new SharedPref(context);
        //Log.d("GeoFenceR","Trigger");
        //Toast.makeText(context, "Hello", Toast.LENGTH_SHORT).show();
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
//            String errorMessage = GeofenceErrorMessages.getErrorString(this,
//                    geofencingEvent.getErrorCode());
            Log.e(Tag, "Error");
            return;
        }

        // Get the transition type.
        int geofenceTransition = geofencingEvent.getGeofenceTransition();

        // Test that the reported transition was of interest.
        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {

            // Get the geofences that were triggered. A single event can trigger
            // multiple geofences.
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

            // Get the transition details as a String.
            String geofenceTransitionDetails = getGeofenceTrasitionDetails(
                    geofenceTransition,
                    triggeringGeofences
            ,context);
            Log.d("Details",geofenceTransitionDetails);
            String[] geofenceindetail=geofenceTransitionDetails.split(" ");
            if(geofenceindetail[0].equals("exiting")){
                Log.d(Tag,"exiting");
                sendRequest("outside");
            }
            else if(geofenceindetail[0].equals("entering")){
                Log.d(Tag,"entering");
                if(geofenceindetail[geofenceindetail.length-1].equals("home")){
                    Log.d(Tag,"Home");
                    sendRequest("home");
                }
                else if(geofenceindetail[geofenceindetail.length-1].equals("office")){
                    Log.d(Tag,"Office");
                    sendRequest("office");
                }
            }
            showNotification("Geo",geofenceindetail[0]+" "+geofenceindetail[geofenceindetail.length-1]);
           // Log.d(Tag,geofenceTransitionDetails);
            // Send notification and log the transition details.
//            sendNotification(geofenceTransitionDetails);
//            Log.i(TAG, geofenceTransitionDetails);
        } else {
            // Log the error.
//            Log.e(TAG, getString(R.string.geofence_transition_invalid_type,
//                    geofenceTransition));
            Log.e(Tag, "Error1");
        }
    }

    private String getGeofenceTrasitionDetails(int geoFenceTransition, List<Geofence> triggeringGeofences,
                                               Context context) {
        // get the ID of each geofence triggered
        ArrayList<String> triggeringGeofencesList = new ArrayList<>();
        for (Geofence geofence : triggeringGeofences) {
            triggeringGeofencesList.add(geofence.getRequestId());
        }

        String status = null;
        if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            status = "entering";
            //Toast.makeText(context, "Enter"+TextUtils.join( ", ", triggeringGeofencesList), Toast.LENGTH_LONG).show();
        } else if (geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            status = "exiting";
            //Toast.makeText(context, "Exiting"+TextUtils.join( ", ", triggeringGeofencesList), Toast.LENGTH_LONG).show();
        }
        //Log.d("important",status+TextUtils.join( ", ", triggeringGeofencesList));
        return status+" "+TextUtils.join( " ", triggeringGeofencesList);
    }
    private void callBack(){
        mIResult=new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {

            }

            @Override
            public void notifyError(String requestType, VolleyError error) {

            }

            @Override
            public void notifyNoInternet(String requestType, String url, HashMap map, String token) {

            }
        };
    }
    private void sendRequest(String status){
        if(ConnectivityCheck.getInstance().isConnected(App.getContext())){
            HashMap map=new HashMap();
            map.put("status",status);
            mVolley.postDataWithAuthorization("status",
                    "changestatus",map,pref.getToken());
        }
        else{
            NoInternetPref.getInstance().setRequest(status);
        }
    }
    private void showNotification(String type,String descr){

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(App.getContext(), "1")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(type)
                        .setContentText(descr);
        mNotifyManager.notify(1,mBuilder.build());

    }

}
