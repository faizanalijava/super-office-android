package com.supertec.superofficeapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.EmployeeLocation;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.services.LiveLocationService;
import com.supertec.superofficeapp.services.PermanentService;

/**
 * Created by Programmer on 1/22/2018.
 */

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(App.getInstance().getPref().getToken()!=null&&App.getInstance().getPref().getQbid()!=null){
            if(App.getInstance().getPref().getUser_Type().equals("employee")){
                if(Build.VERSION.SDK_INT<26){
                    context.startService(new Intent(context,PermanentService.class));
                    context.startService(new Intent(context,LiveLocationService.class));
                }
                else {
                    EmployeeLocation.getInstance().AddGeoFences();
                }
            }
        }
    }
}
