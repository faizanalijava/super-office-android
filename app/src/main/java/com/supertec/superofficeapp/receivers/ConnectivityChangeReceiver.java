package com.supertec.superofficeapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.VolleyError;
import com.supertec.superofficeapp.ConstantClasses.App;
import com.supertec.superofficeapp.ConstantClasses.ConnectivityCheck;
import com.supertec.superofficeapp.ConstantClasses.IResult;
import com.supertec.superofficeapp.ConstantClasses.SharedPref;
import com.supertec.superofficeapp.ConstantClasses.VolleyService;
import com.supertec.superofficeapp.SharedPreferences.NoInternetPref;
import java.util.HashMap;

/**
 * Created by Programmer on 1/21/2018.
 */

public class ConnectivityChangeReceiver extends BroadcastReceiver {
    private static VolleyService mVolley;
    private static IResult mIResult;
    SharedPref pref;
    @Override
    public void onReceive(Context context, Intent intent) {
       //Toast.makeText(context, ""+NoInternetPref.getInstance().getRequest(), Toast.LENGTH_SHORT).show();
        callBack();
        mVolley=new VolleyService(mIResult,context);
        pref=new SharedPref(context);
        if(pref.getToken()!=null&&pref.getUser_Type().equals("employee")){
            {
                Log.d("imp","here0");
                if (ConnectivityCheck.getInstance().isConnected(context)) {
                    Log.d("imp","here");
                    if (NoInternetPref.getInstance().getRequest() != null) {
                        sendRequest(NoInternetPref.getInstance().getRequest());
                    }
                } else {

                }
            }

    }
    }
    private void callBack(){
        mIResult=new IResult() {
            @Override
            public void notifySuccess(String requestType, String response) {
                Log.d("imp","here2");
                NoInternetPref.getInstance().clearEverything();
            }

            @Override
            public void notifyError(String requestType, VolleyError error) {

            }

            @Override
            public void notifyNoInternet(String requestType, String url, HashMap map, String token) {

            }
        };
    }
    private void sendRequest(String status){
        if(ConnectivityCheck.getInstance().isConnected(App.getContext())){
            Log.d("imp","here1");
            HashMap map=new HashMap();
            map.put("status",status);
            mVolley.postDataWithAuthorization("status",
                    "changestatus",map,pref.getToken());
        }
        else{
            NoInternetPref.getInstance().setRequest(status);
        }
    }

}
